package modelo.reportajes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import POJOs.DietaPOJO;
import POJOs.EmpresaPOJO;
import POJOs.EquipoPOJO;
import POJOs.EventoDisplayPOJO;
import POJOs.EventoPOJO;
import POJOs.InformeEventosReporteroPOJO;
import POJOs.InformeReportajePOJO;
import POJOs.InformeReporterosPOJO;
import POJOs.LugarPOJO;
import POJOs.MultimediaPOJO;
import POJOs.ReporteroPOJO;
import POJOs.agenciaPrensaPOJO;
import POJOs.OfrecimientoPOJO;
import POJOs.ReportajePOJO;
import util.reportajes.Database;

public class modelo {
	private Database db=new Database();

	public void inicializar(boolean singleton) {
		db.createDatabase(singleton);
		db.loadDatabase();
	}



	public List<OfrecimientoPOJO> getOfrecimientos(String id_empresa) {
		String SQL_OFRECIMIENTOS_EMPRESA ="SELECT evento.id_evento,evento.titulo,agencia_prensa.id_agencia,evento.precio,tema.nombre AS tematica"
				+" FROM ofrecimiento INNER JOIN evento ON ofrecimiento.id_evento=evento.id_evento"
				+" INNER JOIN agencia_prensa ON agencia_prensa.id_agencia=evento.id_agencia"
				+" INNER JOIN temaevento ON temaevento.id_evento=evento.id_evento"
				+" INNER JOIN tema ON tema.id_tema=temaevento.id_tema "
				+" WHERE ofrecimiento.id_empresa=? AND ofrecimiento.aceptado IS NULL"
				+" ORDER BY evento.id_evento";
				
				return db.executeQueryPojo(OfrecimientoPOJO.class, SQL_OFRECIMIENTOS_EMPRESA, id_empresa);
	}
	
	public List<OfrecimientoPOJO> getOfrecimientosMax(String id_empresa, int maximo) {
		String SQL_OFRECIMIENTOS_EMPRESA ="SELECT evento.id_evento,evento.titulo,agencia_prensa.id_agencia,evento.precio, tema.nombre AS tematica "
				+" FROM ofrecimiento INNER JOIN evento ON ofrecimiento.id_evento=evento.id_evento"
				+" INNER JOIN agencia_prensa ON agencia_prensa.id_agencia=evento.id_agencia"
				+" INNER JOIN temaevento ON temaevento.id_evento=evento.id_evento"
				+" INNER JOIN tema ON tema.id_tema=temaevento.id_tema "
				+" WHERE ofrecimiento.id_empresa=? AND ofrecimiento.aceptado IS NULL AND evento.precio <= ?"
				+" ORDER BY evento.id_evento";
		
				return db.executeQueryPojo(OfrecimientoPOJO.class, SQL_OFRECIMIENTOS_EMPRESA, id_empresa,maximo);
	}
	
	public List<OfrecimientoPOJO> getOfrecimientosMin(String id_empresa, int minimo) {
		String SQL_OFRECIMIENTOS_EMPRESA ="SELECT evento.id_evento,evento.titulo,agencia_prensa.id_agencia,evento.precio,tema.nombre AS tematica "
				+" FROM ofrecimiento INNER JOIN evento ON ofrecimiento.id_evento=evento.id_evento"
				+" INNER JOIN agencia_prensa ON agencia_prensa.id_agencia=evento.id_agencia"
				+" INNER JOIN temaevento ON temaevento.id_evento=evento.id_evento"
				+" INNER JOIN tema ON tema.id_tema=temaevento.id_tema "
				+" WHERE ofrecimiento.id_empresa=? AND ofrecimiento.aceptado IS NULL AND evento.precio >= ?"
				+" ORDER BY evento.id_evento";
				
				return db.executeQueryPojo(OfrecimientoPOJO.class, SQL_OFRECIMIENTOS_EMPRESA, id_empresa,minimo);
	}
	
	public List<OfrecimientoPOJO> getOfrecimientosMaxMin(String id_empresa, int maximo, int minimo) {
		String SQL_OFRECIMIENTOS_EMPRESA ="SELECT evento.id_evento,evento.titulo,agencia_prensa.id_agencia,evento.precio, tema.nombre AS tematica"
				+" FROM ofrecimiento INNER JOIN evento ON ofrecimiento.id_evento=evento.id_evento"
				+" INNER JOIN agencia_prensa ON agencia_prensa.id_agencia=evento.id_agencia"
				+" INNER JOIN temaevento ON temaevento.id_evento=evento.id_evento"
				+" INNER JOIN tema ON tema.id_tema=temaevento.id_tema "
				+" WHERE ofrecimiento.id_empresa=? AND ofrecimiento.aceptado IS NULL AND evento.precio <= ? AND evento.precio >= ?"
				+" ORDER BY evento.id_evento";
				
				return db.executeQueryPojo(OfrecimientoPOJO.class, SQL_OFRECIMIENTOS_EMPRESA, id_empresa,maximo,minimo);
	}
	
	

	//Devuelve los eventos de una agencia
	public List<EventoDisplayPOJO> getEventosTabla(int id_agencia) {
		String SQL_EVENTOS_SIN_REPORTERO="SELECT evento.id_evento,titulo FROM agencia_prensa"
				+ " INNER JOIN evento ON evento.id_agencia = agencia_prensa.id_agencia "
				+ " WHERE evento.equipo_terminado = 1 AND agencia_prensa.id_agencia = ?";
		return db.executeQueryPojo(EventoDisplayPOJO.class, SQL_EVENTOS_SIN_REPORTERO,id_agencia);
	}

	public List<EmpresaPOJO> getEmpresasCom_Tabla(String id_evento) {
		String SQL_LISTA_EMPRESAS="SELECT empresa_comunicacion.id_empresa,nombre,tarifa.tipo, tarifa.pagado AS pagada "
				+ " from empresa_comunicacion"
				+ " LEFT JOIN ofrecimiento ON empresa_comunicacion.id_empresa = ofrecimiento.id_empresa "
				+ " LEFT JOIN tarifa ON tarifa.id_empresa = empresa_comunicacion.id_empresa"
				+ " WHERE empresa_comunicacion.id_empresa "
				+ " NOT IN"
				+ "(SELECT id_empresa FROM ofrecimiento WHERE id_evento = ?)"
				+ "GROUP BY empresa_comunicacion.id_empresa";
		return db.executeQueryPojo(EmpresaPOJO.class, SQL_LISTA_EMPRESAS,id_evento);
	}
	
	public List<EmpresaPOJO> getEmpresasCom_TablaIsIn(String id_evento) {
		String SQL_LISTA_EMPRESAS="SELECT empresa_comunicacion.id_empresa,nombre,tarifa.tipo, tarifa.pagado AS pagada "
				+ " from empresa_comunicacion"
				+ " LEFT JOIN ofrecimiento ON empresa_comunicacion.id_empresa = ofrecimiento.id_empresa "
				+ " LEFT JOIN tarifa ON tarifa.id_empresa = empresa_comunicacion.id_empresa"
				+ " WHERE empresa_comunicacion.id_empresa "
				+ " IN"
				+ "(SELECT id_empresa FROM ofrecimiento WHERE id_evento = ?)"
				+ " GROUP BY empresa_comunicacion.id_empresa"
				+ " ORDER BY id_ofrecimiento DESC";
		return db.executeQueryPojo(EmpresaPOJO.class, SQL_LISTA_EMPRESAS,id_evento);
	}

	public void insertaOfrecimientos(int id_ofrecimiento, int id_evento,int id_empresa) {
		String SQL_INSERTA_OFRECIMIENTOS="INSERT INTO ofrecimiento (id_ofrecimiento,aceptado,acceso,id_evento,id_empresa,pagado)"
				+ "VALUES (?,NULL,0,?,?,0)";
		db.executeUpdate(SQL_INSERTA_OFRECIMIENTOS,id_ofrecimiento,id_evento,id_empresa);

	}
	
	public void insertaOfrecimientosPLANA(int id_ofrecimiento, int id_evento,int id_empresa) {
		String SQL_INSERTA_OFRECIMIENTOS="INSERT INTO ofrecimiento (id_ofrecimiento,aceptado,acceso,id_evento,id_empresa,pagado)"
				+ "VALUES (?,NULL,0,?,?,1)";
		db.executeUpdate(SQL_INSERTA_OFRECIMIENTOS,id_ofrecimiento,id_evento,id_empresa);

	}
	
	public List<EmpresaPOJO> getEmpresaId(String id_evento, String id_empresa) {
		String SQL_LISTA_EMPRESAS="SELECT empresa_comunicacion.id_empresa,nombre,tarifa.tipo, tarifa.pagado "
				+ " from empresa_comunicacion"
				+ " LEFT JOIN ofrecimiento ON empresa_comunicacion.id_empresa = ofrecimiento.id_empresa "
				+ " LEFT JOIN tarifa ON tarifa.id_empresa = empresa_comunicacion.id_empresa"
				+ " WHERE empresa_comunicacion.id_empresa "
				+ " NOT IN"
				+ "(SELECT id_empresa FROM ofrecimiento WHERE id_evento = ? AND id_empresa = ?)"
				+ "GROUP BY empresa_comunicacion.id_empresa";
		return db.executeQueryPojo(EmpresaPOJO.class, SQL_LISTA_EMPRESAS,id_evento,id_empresa);
	}

	public List<EventoDisplayPOJO> getEventos_Tabla_Con_Reportero() {
		String SQL_EVENTOS="SELECT evento.id_evento,titulo FROM evento"
				+ " LEFT JOIN equipo ON equipo.id_evento = evento.id_evento" +
				" WHERE id_reportero IS NOT NULL GROUP BY evento.id_evento";

		return db.executeQueryPojo(EventoDisplayPOJO.class, SQL_EVENTOS);
	}

	public List<EventoPOJO> getEventos_Tabla() {
		String SQL_EVENTOS="SELECT evento.id_evento,titulo,diaInicio,diaFin FROM evento"
				+ " LEFT JOIN equipo ON equipo.id_evento = evento.id_evento" +
				" WHERE id_reportero IS NULL";

		return db.executeQueryPojo(EventoPOJO.class, SQL_EVENTOS);
	}

	public List<EventoPOJO> getEventosSINREP_Tabla() {
		String SQL_EVENTOS="SELECT evento.id_evento,titulo,diaInicio FROM evento"
				+ " LEFT JOIN equipo ON equipo.id_evento = evento.id_evento" +
				" WHERE id_reportero IS NOT NULL";

		return db.executeQueryPojo(EventoPOJO.class, SQL_EVENTOS);
	}


	public List<ReporteroPOJO> getRepDis_TablaRolResidentes(String FechaInicio,String rol,String region,String FechaFin,String idEvento,int TipoContrato) {
		
		String SQL_LISTA_REP_DIS="SELECT reportero.id_reportero,reportero.nombre"
				+" FROM reportero"
				+" INNER JOIN rolreportero ON reportero.id_reportero = rolreportero.id_reportero"
				+" INNER JOIN rol ON rolreportero.id_rol = rol.id_rol"
				+" INNER JOIN temareportero ON temareportero.id_reportero=reportero.id_reportero"
				+" INNER JOIN lugar ON lugar.id_lugar=reportero.id_lugar"
				+" WHERE ((rol.nombre =?) "
				+" AND (lugar.region=?)"
				+" AND ((reportero.id_contrato=?) OR (reportero.id_contrato=3))"
				+" AND temareportero.id_tema IN"
				+" (SELECT temaevento.id_tema from temaevento INNER JOIN evento ON (evento.id_evento=temaevento.id_evento) WHERE evento.id_evento=?))"
				+" EXCEPT"
				+" SELECT reportero.id_reportero,reportero.nombre"
				+" FROM reportero"
				+" INNER JOIN equipo ON reportero.id_reportero = equipo.id_reportero"
				+" INNER JOIN evento ON equipo.id_evento = evento.id_evento"
				+" WHERE (((evento.diaInicio >= ?) AND (evento.diaInicio <= ?)) OR"
				+" ((evento.diaFin >= ?) AND (evento.diaFin <= ?)) OR"
				+" ((evento.diaInicio <= ?) AND (evento.diaFin >= ?)) OR"
				+" ((evento.diaInicio >= ?) AND (evento.diaFin <= ?)))";
				
		return db.executeQueryPojo(ReporteroPOJO.class, SQL_LISTA_REP_DIS,rol,region,TipoContrato,idEvento,FechaInicio,FechaFin,FechaInicio,FechaFin,FechaInicio,FechaFin,FechaInicio,FechaFin);
	}

	public List<ReporteroPOJO> getRepDis_TablaRol(String FechaInicio,String rol,String FechaFin,String idEvento,int TipoContrato) {

		String SQL_LISTA_REP_DIS="SELECT reportero.id_reportero,reportero.nombre"
				+" FROM reportero"
				+" INNER JOIN rolreportero ON reportero.id_reportero = rolreportero.id_reportero"
				+" INNER JOIN rol ON rolreportero.id_rol = rol.id_rol"
				+" INNER JOIN temareportero ON temareportero.id_reportero=reportero.id_reportero"
				+" WHERE ((rol.nombre =?) "
				+" AND ((reportero.id_contrato=?) OR (reportero.id_contrato=3))"
				+" AND temareportero.id_tema IN"
				+" (SELECT temaevento.id_tema from temaevento INNER JOIN evento ON (evento.id_evento=temaevento.id_evento) WHERE evento.id_evento=?))"
				+" EXCEPT"
				+" SELECT reportero.id_reportero,reportero.nombre"
				+" FROM reportero"
				+" INNER JOIN equipo ON reportero.id_reportero = equipo.id_reportero"
				+" INNER JOIN evento ON equipo.id_evento = evento.id_evento"
				+" WHERE (((evento.diaInicio >= ?) AND (evento.diaInicio <= ?)) OR"
				+" ((evento.diaFin >= ?) AND (evento.diaFin <= ?)) OR"
				+" ((evento.diaInicio <= ?) AND (evento.diaFin >= ?)) OR"
				+" ((evento.diaInicio >= ?) AND (evento.diaFin <= ?)))";

		return db.executeQueryPojo(ReporteroPOJO.class, SQL_LISTA_REP_DIS,rol,TipoContrato,idEvento,FechaInicio,FechaFin,FechaInicio,FechaFin,FechaInicio,FechaFin,FechaInicio,FechaFin);
	}
	
	public List<ReporteroPOJO> getRepDis_TablaRolEnZona(String FechaInicio,String rol,String Region,String FechaFin,String idEvento,int TipoContrato,String FechaInicioAnterior) {

		String SQL_LISTA_REP_DIS="SELECT reportero.id_reportero,reportero.nombre"
				+" FROM reportero"
				+" INNER JOIN rolreportero ON reportero.id_reportero = rolreportero.id_reportero"
				+" INNER JOIN rol ON rolreportero.id_rol = rol.id_rol"
				+" INNER JOIN temareportero ON temareportero.id_reportero=reportero.id_reportero"
				+" INNER JOIN equipo ON reportero.id_reportero = equipo.id_reportero"
				+" INNER JOIN evento ON equipo.id_evento = evento.id_evento"
				+" INNER JOIN lugar ON lugar.id_lugar=evento.id_lugar"
				+" WHERE ((rol.nombre =?) "
				+" AND evento.diaFin=? AND lugar.region=?"
				+" AND ((reportero.id_contrato=?) OR (reportero.id_contrato=3))"
				+" AND temareportero.id_tema IN"
				+" (SELECT temaevento.id_tema from temaevento INNER JOIN evento ON (evento.id_evento=temaevento.id_evento) WHERE evento.id_evento=?))"
				+" EXCEPT"
				+" SELECT reportero.id_reportero,reportero.nombre"
				+" FROM reportero"
				+" INNER JOIN equipo ON reportero.id_reportero = equipo.id_reportero"
				+" INNER JOIN evento ON equipo.id_evento = evento.id_evento"
				+" WHERE (((evento.diaInicio >= ?) AND (evento.diaInicio <= ?)) OR"
				+" ((evento.diaFin >= ?) AND (evento.diaFin <= ?)) OR"
				+" ((evento.diaInicio <= ?) AND (evento.diaFin >= ?)) OR"
				+" ((evento.diaInicio >= ?) AND (evento.diaFin <= ?)))";

		return db.executeQueryPojo(ReporteroPOJO.class, SQL_LISTA_REP_DIS,rol,FechaInicioAnterior,Region,TipoContrato,idEvento,FechaInicio,FechaFin,FechaInicio,FechaFin,FechaInicio,FechaFin,FechaInicio,FechaFin);
	}


	public List<Object[]> getRolComboBox() {
		String SQL_ROL="SELECT nombre FROM rol";

		return db.executeQueryArray(SQL_ROL);
	}
	
	public List<Object[]> getTematicaComboBox() {
		String SQL_ROL="SELECT nombre FROM tema";

		return db.executeQueryArray(SQL_ROL);
	}

	public List<ReporteroPOJO> getRepEnEquipo_Tabla(String idEvento) {
		String SQL_LISTA_REP_EN_EQUIPO="SELECT reportero.id_reportero,nombre"
				+" FROM reportero"
				+" INNER JOIN equipo ON reportero.id_reportero = equipo.id_reportero"
				+" WHERE equipo.id_evento=?";
		return db.executeQueryPojo(ReporteroPOJO.class, SQL_LISTA_REP_EN_EQUIPO,idEvento);
	}

	public List<ReporteroPOJO> getRepEnEquipo_TablaRoles(String idEvento) {
		String SQL_LISTA_REP_EN_EQUIPO="SELECT reportero.id_reportero,reportero.nombre,rol.nombre as rol"
				+" FROM reportero"
				+" INNER JOIN equipo ON reportero.id_reportero = equipo.id_reportero"
				+" INNER JOIN rolreportero ON reportero.id_reportero = rolreportero.id_reportero"
				+" INNER JOIN rol ON rolreportero.id_rol = rol.id_rol"
				+" WHERE equipo.id_evento=?";
		return db.executeQueryPojo(ReporteroPOJO.class, SQL_LISTA_REP_EN_EQUIPO,idEvento);
	}

	public List<EquipoPOJO> getRepEnEquipo_TablaRes(String idEvento) {
		String SQL_LISTA_REP_EN_EQUIPO="SELECT equipo.id_reportero,equipo.responsable"
				+" FROM equipo"
				+" WHERE equipo.id_evento=?";
		return db.executeQueryPojo(EquipoPOJO.class, SQL_LISTA_REP_EN_EQUIPO,idEvento);
	}



	public void TerminarEquipo(String id_evento) {
		String SQL_NoRes = "UPDATE evento SET"
				+" equipo_terminado=1 WHERE id_evento=?";
		db.executeUpdate(SQL_NoRes,id_evento);	
	}

	/**
	 * obtiene los eventos asociados a un reportero sin reportaje
	 * @return lista de eventos
	 */
	public List<EventoDisplayPOJO> getEventosSinReportaje(String id) {


		String sql2="SELECT evento.id_evento, evento.titulo, evento.diaInicio,evento.diaFin,evento.descripcion FROM evento"
				+ " INNER JOIN equipo ON (evento.id_evento=equipo.id_evento)"
				+ " INNER JOIN reportero ON (equipo.id_reportero=reportero.id_reportero)"
				+ " WHERE reportero.id_reportero=?"
				+ " AND id_reportaje IS NULL";


		return db.executeQueryPojo(EventoDisplayPOJO.class, sql2,id);
	}

	public List<EventoDisplayPOJO> getEventosConReportajesNoFinalizados(String id) {


		String sql2="SELECT evento.id_evento, evento.titulo, evento.diaInicio,evento.diaFin,evento.descripcion FROM evento"
				+ " INNER JOIN equipo ON (evento.id_evento=equipo.id_evento)"
				+ " INNER JOIN reportero ON (equipo.id_reportero=reportero.id_reportero)"
				+ " INNER JOIN reportaje ON (evento.id_reportaje=reportaje.id_reportaje)"
				+ " WHERE reportero.id_reportero=?"
				+ " AND reportaje.finalizado=0";


		return db.executeQueryPojo(EventoDisplayPOJO.class, sql2,id);
	}

	public List<ReporteroPOJO> getReporteros(String evento) {
		String SQL_LISTA_REP_EN_EQUIPO="SELECT nombre"
				+" FROM reportero"
				+" INNER JOIN equipo ON reportero.id_reportero = equipo.id_reportero"
				+" WHERE equipo.id_evento=?";
		return db.executeQueryPojo(ReporteroPOJO.class, SQL_LISTA_REP_EN_EQUIPO,evento);
	}

	public void terminarReportaje(String idEvento) {
		String SQL_TerminarReportaje = "UPDATE reportaje SET finalizado=1 WHERE id_reportaje =  (SELECT reportaje.id_reportaje FROM reportaje INNER JOIN evento ON  reportaje.id_reportaje = evento.id_reportaje WHERE evento.id_evento = ?)";

		db.executeUpdate(SQL_TerminarReportaje,idEvento);

	}

	public List<EquipoPOJO> obtieneReporterosEquipo(String id_evento) {
		String SQL_Consulta = "SELECT id_reportero,revisado FROM equipo WHERE id_evento = ?";

		return db.executeQueryPojo(EquipoPOJO.class, SQL_Consulta,id_evento);

	}



	public void anadirReportero(String idEvento,String idReportero) {
		String SQL_ANADIR = "INSERT INTO equipo (id_evento,id_reportero,responsable)"
				+" VALUES (?,?,0)";
		db.executeUpdate(SQL_ANADIR,idEvento,idReportero);
	}

	public void hacerResponsable(String idResponsable,String idEvento) {
		String SQL_NoRes = "UPDATE equipo SET"
				+" responsable=0 WHERE id_evento=?";
		db.executeUpdate(SQL_NoRes,idEvento);		

		String SQL_AnadirResponsable = "UPDATE equipo SET"
				+" responsable=1 WHERE id_reportero=? AND id_evento=?";
		db.executeUpdate(SQL_AnadirResponsable,idResponsable,idEvento);

	}

	//Aceptado valdr� 1 si el ofrecimiento es aceptado o 0 si es rechazado
	public void respondeOfrecimiento(String id_evento, String id_agencia, String aceptado,int empresa) {
		String SQL_RESPONDE_OFRECIMIENTOS = "UPDATE ofrecimiento"+
				" SET aceptado = ?" +
				"WHERE id_evento = ? AND id_empresa = ?";
		db.executeUpdate(SQL_RESPONDE_OFRECIMIENTOS, aceptado,id_evento,empresa);


	}

	/**
	 * Inserta el reportaje en la tabla y enlaza el reportaje con su evento.
	 * @param id_reportaje
	 * @param Titulo
	 * @param Subtitulo
	 * @param Cuerpo
	 * @param id_evento
	 */
	public void insertaReportaje(int id_reportaje,String Titulo,String Subtitulo, String Cuerpo,int Finalizado,int id_evento) {
		String SQL_INSERTA_REPORTAJE="INSERT INTO reportaje (id_reportaje,titulo,subtitulo,cuerpo,finalizado) VALUES (?,?,?,?,?)";
		String SQL_ACTUALIZA_EVENTO="UPDATE evento SET id_reportaje = ? WHERE id_evento=?";
		db.executeUpdate(SQL_INSERTA_REPORTAJE,id_reportaje,Titulo,Subtitulo,Cuerpo,Finalizado);
		db.executeUpdate(SQL_ACTUALIZA_EVENTO,id_reportaje,id_evento);
	}

	/**
	 * obtiene los ids de todos los reportajes
	 * @return lista con los ids
	 */
	public List<ReportajePOJO> listaReportajes() {
		String SQL="SELECT id_reportaje FROM reportaje";
		return db.executeQueryPojo(ReportajePOJO.class, SQL);
	}

	/**
	 * Obtiene titulo subtitulo y cuerpo de un reportaje
	 * @param id_evento
	 * @return una lista de reportajes con un uncico reportaje
	 */
	public List<ReportajePOJO> getReportaje(String id_evento) {
		String sql="SELECT reportaje.titulo, reportaje.subtitulo, reportaje.cuerpo FROM"
				+ " reportaje"
				+ " INNER JOIN evento ON (reportaje.id_reportaje = evento.id_reportaje)"
				+ " WHERE evento.id_evento = ?";
		return db.executeQueryPojo(ReportajePOJO.class, sql,id_evento);
	}

	/**
	 * Obtiene datos de los eventos que se han ofrecido a una empresa y la empresa los ha aceptado
	 * @param id_empresa
	 * @return lista de EventosDisplayPOJO 
	 */
	public List<EventoDisplayPOJO> getEventos(String id_empresa) {
		String sql2="SELECT evento.id_evento, evento.titulo, evento.diaInicio, evento.descripcion FROM evento"
				+ " INNER JOIN ofrecimiento ON (evento.id_evento=ofrecimiento.id_evento)"
				+ " WHERE ofrecimiento.aceptado = 1 AND ofrecimiento.id_empresa=? AND evento.id_reportaje IS NOT NULL";
		return db.executeQueryPojo(EventoDisplayPOJO.class, sql2,id_empresa);

	}

	public List<EventoPOJO> getEventosConReportaje_Tabla() {
		String SQL_EVENTOS="SELECT evento.id_evento,titulo FROM evento"
				+" WHERE id_reportaje IS NOT NULL";

		return db.executeQueryPojo(EventoPOJO.class, SQL_EVENTOS);
	}

	public List<EventoPOJO> getEventosConReportajeTerminado_Tabla() {
		String SQL_EVENTOS="SELECT evento.id_evento,evento.titulo FROM evento"
				+" INNER JOIN reportaje ON reportaje.id_reportaje=evento.id_reportaje"
				+" WHERE reportaje.finalizado=1";

		return db.executeQueryPojo(EventoPOJO.class, SQL_EVENTOS);
	}

	public List<EmpresaPOJO> getEmpresasAcepOfre_Tabla(String key) {
		String SQL_EVENTOS="SELECT empresa_comunicacion.id_empresa,empresa_comunicacion.nombre FROM empresa_comunicacion"
				+" INNER JOIN ofrecimiento ON ofrecimiento.id_empresa=empresa_comunicacion.id_empresa"
				+" INNER JOIN evento ON ofrecimiento.id_evento=evento.id_evento"
				+" WHERE evento.id_evento=? AND ofrecimiento.aceptado=1 AND ofrecimiento.acceso=0";

		return db.executeQueryPojo(EmpresaPOJO.class, SQL_EVENTOS,key);
	}

	public void PermitirAcceso(String idEvento,String idEmpresa) {
		String SQL_EVENTOS="UPDATE ofrecimiento"
				+" SET acceso=1"
				+" WHERE id_Evento=? AND id_empresa=?";

		db.executeUpdate(SQL_EVENTOS,idEvento,idEmpresa);
		System.out.printf("La empresa %s tiene acceso al reportaje del evento %s \n",idEmpresa,idEvento);
	}


	/**
	 * Obtiene los ids de la tabla multimedia
	 * Necesario para saber el �ltimo
	 * @return lista de ids en objetos MultimediaPOJO
	 */
	public List<MultimediaPOJO> getIdsMultimedia() {
		String sql="SELECT id_multimedia FROM multimedia";
		return db.executeQueryPojo(MultimediaPOJO.class, sql);
	}


	/**
	 * Consulta para actualizar tabla de multimedia
	 * @param multimedia
	 */
	public void insertaMultimedia(List<MultimediaPOJO> multimedia) {
		String id_multimedia;
		String id_reportaje;

		for(MultimediaPOJO m: multimedia) {
			id_multimedia=m.getId_multimedia();
			id_reportaje=m.getId_reportaje();
			String sql="INSERT INTO multimedia (id_multimedia,tipo,ruta,id_reportaje)"
					+ "VALUES (?,?,?,?)";
			db.executeUpdate(sql, id_multimedia,m.getTipo(),m.getRuta(),id_reportaje);
		}


	}
	/**
	 * Obtiene un contenido multimedi a partir de la ruta
	 * @param ruta
	 * @return retorna una lista de 1 elemento (la imagen o video con la ruta especificada)
	 */
	public List<MultimediaPOJO> getMultimedia(String ruta) {
		String sql="SELECT id_multimedia, tipo, ruta FROM multimedia WHERE ruta=?";
		return db.executeQueryPojo(MultimediaPOJO.class, sql,ruta);

	}

	/**
	 * Obtiene un contenido multimedi a partir del evento
	 * @param id_evento
	 * @return retorna una lista de multimedia 
	 */
	public List<MultimediaPOJO> getMultimediaConIdEvento(String id_evento) {
		String sql="SELECT id_multimedia, tipo, ruta FROM multimedia"
				+ " INNER JOIN reportaje ON (reportaje.id_reportaje = multimedia.id_reportaje)"
				+ " INNER JOIN evento ON (evento.id_reportaje=reportaje.id_reportaje)"
				+ " WHERE evento.id_evento=?";
		return db.executeQueryPojo(MultimediaPOJO.class, sql,id_evento);
	}


	/**
	 * obtiene los eventos sin reportaje en los que el reportero es responsable
	 * @param id_reportero
	 * @return lista de eventosDisplayPOJO
	 */
	public List<EventoDisplayPOJO> getEventosSinReportajeEsResponsable(String id_reportero) {

		String sql2="SELECT * FROM evento"
				+ " INNER JOIN equipo ON (evento.id_evento=equipo.id_evento)"
				+ " INNER JOIN reportero ON (equipo.id_reportero=reportero.id_reportero)"
				+ " WHERE reportero.id_reportero=?"
				+ " AND equipo.responsable = 1";


		return db.executeQueryPojo(EventoDisplayPOJO.class, sql2,id_reportero);
	}

	/**
	 * obtiene los eventos con reportaje en los que el reportero es responsable
	 * @param id_reportero
	 * @return lista de eventosDisplayPOJO
	 */
	public List<EventoDisplayPOJO> getEventosConReportajeEsResponsable(String id_reportero) {

		String sql2="SELECT * FROM evento"
				+ " INNER JOIN equipo ON (evento.id_evento=equipo.id_evento)"
				+ " INNER JOIN reportero ON (equipo.id_reportero=reportero.id_reportero)"
				+ " WHERE reportero.id_reportero= ?"
				+ " AND equipo.responsable = 1 AND evento.id_reportaje IS NOT NULL";


		return db.executeQueryPojo(EventoDisplayPOJO.class, sql2,id_reportero);
	}

	/**
	 * Obtiene el comentario realizado de un reportero y si est� revisado o no
	 * @param id_reportero
	 * @param id_evento
	 * @return
	 */
	public EquipoPOJO getEquipoComentario(String id_evento, String id_reportero) {
		String sql="SELECT * FROM equipo"
				+ " WHERE id_reportero = ? AND id_evento=?";
		List<EquipoPOJO> a= db.executeQueryPojo(EquipoPOJO.class, sql,id_reportero, id_evento);
		if(a.size()==0)
			return null;
		return a.get(0);
	}



	/** 
	 * Actualiza el comentario hecho por un reportero
	 * @param equipo
	 * @param id_evento
	 * @param id_reportero
	 */
	public void guardaComentario(EquipoPOJO e, String id_evento, String id_reportero) {
		String sql= "UPDATE equipo SET"
				+" comentario=?, revisado=? WHERE id_evento=? AND id_reportero=?";
		String comentario=e.getComentario();
		int revisado=e.getRevisado();

		db.executeUpdate(sql,comentario,revisado,id_evento,id_reportero);

	}



	/**
	 * Obtiene los comentarios asociados a un evento
	 * @param id_evento
	 * @return
	 */
	public List<EquipoPOJO> getComentarios(String id_evento) {
		String sql="SELECT equipo.id_reportero, equipo.comentario, equipo.revisado FROM equipo"
				+ " WHERE id_evento=? AND comentario IS NOT NULL";
		return db.executeQueryPojo(EquipoPOJO.class, sql, id_evento);
	}


	/**
	 * Obitene el id del reportaje asociado al evento id_evento
	 * @param id_evento
	 * @return
	 */

	public String getIdReportajeEvento(String id_evento) {
		String sql="SELECT reportaje.id_reportaje FROM reportaje"
				+ " INNER JOIN evento ON (evento.id_reportaje = reportaje.id_reportaje)"
				+ "WHERE evento.id_evento=?";
		List<ReportajePOJO> reportaje = db.executeQueryPojo(ReportajePOJO.class, sql, id_evento);
		return reportaje.get(0).getId_reportaje();
	}



	/**
	 * Desvincula la multimedia m del reportaje que tenga asociado
	 * @param m
	 */
	public void actulizarMultimedia(MultimediaPOJO m) {

		//Eliminar la multimedia de la bbdd
		String sql="UPDATE multimedia SET"
				+ " id_reportaje = NULL WHERE id_multimedia=?";
		db.executeUpdate(sql,m.getId_multimedia());	

		//Volver a introducirla con los mismos datos pero si estar asociada
	}
	/**
	 * Actualiza los campos de un reportaje en la bbdd
	 * @param reportaje
	 */
	public void actualizaReportaje(ReportajePOJO reportaje) {
		String sql="UPDATE reportaje SET"
				+ " titulo = ?,subtitulo = ?, cuerpo= ?"
				+ " WHERE id_reportaje= ?";
		db.executeUpdate(sql,reportaje.getTitulo(),reportaje.getSubtitulo(),reportaje.getCuerpo(), reportaje.getId_reportaje());	

	}



	/**
	 * Actualiza el id_reportaje de la multimediaPOJO m
	 * @param m
	 * @param id_reportaje
	 */
	public void actulizarMultimediaNueva(MultimediaPOJO m,String id_reportaje) {
		String sql="UPDATE multimedia SET"
				+ " id_reportaje = ? WHERE id_multimedia=?";
		db.executeUpdate(sql,id_reportaje,m.getId_multimedia());	

	}
	/**
	 * Obtiene todos los eventos en los que trabaja un reportero
	 * @param id_reportero
	 * @return lista con los eventos
	 */
	public List<EventoDisplayPOJO> getEventosReportero(String id_reportero) {
		String sql = "SELECT evento.id_evento, evento.titulo, evento.diaInicio, evento.diaFin, evento.descripcion FROM"
				+ " evento INNER JOIN equipo ON(evento.id_evento = equipo.id_evento)"
				+ " INNER JOIN reportero ON(equipo.id_reportero = reportero.id_reportero)"
				+ " WHERE reportero.id_reportero = ?";
		return db.executeQueryPojo(EventoDisplayPOJO.class, sql, id_reportero);
	}
	/**
	 * obtiene la dieta por alimentacion de un evento
	 * @param id_evento
	 * @return un objeto dieta
	 */
	public LugarPOJO getLugar(int id_evento) {
		String sql = "SELECT lugar.id_lugar, lugar.pais, lugar.region, lugar.dieta_alimentacion, lugar.dieta_alojamiento FROM lugar"
				+ " INNER JOIN evento ON (evento.id_lugar = lugar.id_lugar)"
				+ " WHERE id_evento = ?";

		List<LugarPOJO> a =db.executeQueryPojo(LugarPOJO.class, sql, id_evento);
		if(a.size()==0)
			return null;
		return a.get(0);
	}

	/**
	 * Obtiene la region en la que vive un reportero
	 * @param id_reportero
	 * @return un objeto lgar
	 */
	public String getRegionReportero(String id_reportero) {
		String sql= "SELECT lugar.id_lugar, lugar.pais, lugar.region FROM reportero"
				+ " INNER JOIN lugar ON (reportero.id_lugar = lugar.id_lugar)"
				+ " WHERE reportero.id_reportero=? ";
		List<LugarPOJO> a = db.executeQueryPojo(LugarPOJO.class, sql, id_reportero);
		if(a.size()==0)
			return "";
		return a.get(0).getRegion();
	}

	/**
	 * Obtiene la regi�n dodne tiene lugar un evento
	 * @param id_evento
	 * @return un objeto lugar
	 */
	public String getRegionEvento(String id_evento) {
		String sql= "SELECT lugar.id_lugar, lugar.pais, lugar.region FROM evento"
				+ " INNER JOIN lugar ON (evento.id_lugar = lugar.id_lugar)"
				+ " WHERE evento.id_evento=? ";
		List<LugarPOJO> a = db.executeQueryPojo(LugarPOJO.class, sql, id_evento);
		if(a.size()==0)
			return "";
		System.out.printf("Region Evento: %s\n", a.get(0).getRegion());

		return a.get(0).getRegion();
	}


	public List<InformeReportajePOJO> getReportajesConAccesoFiltradoFecha(String id_empresa, String fechaIni, String fechaFin) {


		String sql1="SELECT reportaje.id_reportaje, reportaje.titulo, agencia_prensa.nombre, evento.titulo AS titulo_evento, lugar.pais, "
				+ " lugar.region, evento.diaInicio, evento.diaFin, evento.precio"
				+ " FROM reportaje INNER JOIN evento ON (reportaje.id_reportaje = evento.id_reportaje)"
				+ " INNER JOIN ofrecimiento ON (evento.id_evento = ofrecimiento.id_evento)"
				+ " INNER JOIN empresa_comunicacion ON (empresa_comunicacion.id_empresa = ofrecimiento.id_empresa)"
				+ " INNER JOIN agencia_prensa ON (evento.id_agencia = agencia_prensa.id_agencia)"
				+ " INNER JOIN lugar ON (evento.id_lugar=lugar.id_lugar)"
				+ " WHERE empresa_comunicacion.id_empresa = ? AND ofrecimiento.acceso = 1 AND evento.diaInicio >= ? AND"
				+ " evento.diaFin <= ?";

		return db.executeQueryPojo(InformeReportajePOJO.class, sql1, id_empresa,fechaIni,fechaFin);

	}


	public LugarPOJO getLugarEvento(String id_evento) {
		String sql="SELECT lugar.id_lugar, lugar.pais, lugar.region FROM lugar"
				+ " INNER JOIN evento ON (evento.id_lugar = lugar.id_lugar)"
				+ " WHERE evento.id_evento = ?";
		return db.executeQueryPojo(LugarPOJO.class, sql,id_evento).get(0);
	}


	public List<EmpresaPOJO> getEmpresasAcepOfrePag_Tabla(String key) {
		String SQL_EVENTOS="SELECT empresa_comunicacion.id_empresa,empresa_comunicacion.nombre FROM empresa_comunicacion"
				+" INNER JOIN ofrecimiento ON ofrecimiento.id_empresa=empresa_comunicacion.id_empresa"
				+" INNER JOIN evento ON ofrecimiento.id_evento=evento.id_evento"
				+" WHERE evento.id_evento=? AND ofrecimiento.aceptado=1 AND ofrecimiento.acceso=0 AND ofrecimiento.pagado=1";

		return db.executeQueryPojo(EmpresaPOJO.class, SQL_EVENTOS,key);
	}



	public List<InformeEventosReporteroPOJO> getEventosReporteroFiltrado(String id_reportero, String filtroInicio,
			String filtroFin) {


		String sql1="SELECT evento.id_evento, evento.titulo, lugar.pais, lugar.region, evento.diaInicio, evento.diaFin,"
				+ " lugar.dieta_alimentacion, lugar.dieta_alojamiento FROM evento" 
				+ " INNER JOIN lugar ON (evento.id_lugar=lugar.id_lugar)"
				+ " INNER JOIN equipo ON (equipo.id_evento = evento.id_evento)"
				+ " INNER JOIN reportero ON (reportero.id_reportero = equipo.id_reportero)"
				+ " WHERE reportero.id_reportero = ? AND  evento.diaInicio >= ? AND evento.diaFin <= ?";



		return db.executeQueryPojo(InformeEventosReporteroPOJO.class, sql1, id_reportero,filtroInicio, filtroFin);

	}



	public List<InformeEventosReporteroPOJO> getEventosAgencia(String id_agencia) {
		String sql="SELECT evento.id_evento, evento.titulo, lugar.pais, lugar.region, evento.diaInicio, evento.diaFin FROM evento"
				+ " INNER JOIN lugar ON (evento.id_lugar = lugar.id_lugar)"
				+ " INNER JOIN agencia_prensa ON (evento.id_agencia = agencia_prensa.id_agencia)"
				+ " WHERE agencia_prensa.id_agencia = ?";

		return db.executeQueryPojo(InformeEventosReporteroPOJO.class, sql, id_agencia);
	}



	public List<InformeReporterosPOJO> getInformeReporteros(String id_evento) {
		String sql="SELECT reportero.id_reportero, reportero.nombre, lugar.region,lugar.dieta_alimentacion, lugar.dieta_alojamiento FROM reportero"
				+ " INNER JOIN equipo ON (equipo.id_reportero = reportero.id_reportero)"
				+ " INNER JOIN evento ON (equipo.id_evento = evento.id_evento)"
				+ " INNER JOIN lugar ON (evento.id_lugar = lugar.id_lugar)"
				+ " WHERE evento.id_evento = ?";
		return db.executeQueryPojo(InformeReporterosPOJO.class, sql, id_evento);
	}


}


