package modelo.reportajes;

import lombok.Getter;

@Getter
public class consultas {
	public static final String SQL_EVENTOS_SIN_REPORTERO="SELECT id_evento,titulo,"
			+" from eventos NATURAL JOIN equipo NATURAL JOIN reporteros"
			+ "WHERE id_reportero NOT NULL";
	public static final String SQL_LISTA_EMPRESAS="SELECT id_empresa,nombre,"
			+ "from empresa_comunicacion ";
	public static final String SQL_EVENTOS="SELECT id_evento,titulo,"
			+" from eventos";
	
}
