package POJOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class InformeReportajePOJO {

	private int id_reportaje;
	private String titulo;
	private String nombre;
	private String titulo_evento;
	private String pais;
	private String region;
	private String diaInicio;
	private String diaFin;
	private int precio;
}	
