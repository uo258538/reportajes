package POJOs;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class OfrecimientoPOJO {
	private String id_evento;
	private String titulo;
	private String id_agencia;
	private String precio;
	private String tematica;
	
	
	public OfrecimientoPOJO() {}
	public OfrecimientoPOJO(String id_evento, String titulo, String id_agencia,String precio, String tematica) {
		this.id_evento = id_evento;
		this.titulo = titulo;
		this.id_agencia = id_agencia;
		this.precio=precio;
		this.tematica=tematica;
	}

}
