package POJOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class EventoDisplayPOJO {
	
	private String id_evento;
	private String titulo;
	private String descripcion;
	private String diaInicio;
	private String diaFin;
	
	public EventoDisplayPOJO() {
		
	}
}
