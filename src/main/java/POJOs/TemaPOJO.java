package POJOs;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TemaPOJO {
	private String nombre;
	private int id;
	public TemaPOJO() {};
	public TemaPOJO(String nombre, int id) {
		this.nombre = nombre;
		this.id = id;
	}

}
