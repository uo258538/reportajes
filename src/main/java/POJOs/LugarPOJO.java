package POJOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LugarPOJO {
	
	private int id_lugar;
	private String pais;
	private String region;
	private int dieta_alimentacion;
	private int dieta_alojamiento;
	
	public LugarPOJO() {
		
	}

}
