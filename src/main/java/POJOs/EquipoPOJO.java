package POJOs;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class EquipoPOJO {
	private String id_evento;
	private String id_reportero;
	private String responsable;
	private String comentario;
	private int revisado ;
	public EquipoPOJO() {}
	public EquipoPOJO(String id_evento, String id_reportero,String responsable, String comentario, int finalizado) {
		this.id_evento = id_evento;
		this.id_reportero=id_reportero;
		this.responsable=responsable;
		this.comentario=comentario;
		this.revisado =finalizado;
	}
}
