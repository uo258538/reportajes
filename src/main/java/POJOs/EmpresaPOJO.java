package POJOs;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
public class EmpresaPOJO {
	private String id_empresa;
	private String nombre;
	private String tipo;
	private String pagada;
	
	public EmpresaPOJO() {}
	public EmpresaPOJO(String id_empresa,String nombre, String tipo, String pagado) {
		this.setId_empresa(id_empresa);
		this.setNombre(nombre);
		this.tipo = tipo;
		this.pagada = pagado;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String nombre) {
		this.tipo = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getId_empresa() {
		return id_empresa;
	}
	public void setId_empresa(String id_empresa) {
		this.id_empresa = id_empresa;
	}

}
