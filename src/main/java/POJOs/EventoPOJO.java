package POJOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter @Getter 
public class EventoPOJO {
	private String id_evento;
	private String titulo;
	private String descripcion;
	private String diaInicio;
	private String diaFin;
	private int precio;

	public EventoPOJO() {}
	public EventoPOJO(String id_evento, String titulo,String diaInicio,String diaFin,int precio) {
		this.id_evento = id_evento;
		this.titulo=titulo;
		this.diaInicio=diaInicio;
		this.diaFin=diaFin;
		this.precio=precio;
	}

}
