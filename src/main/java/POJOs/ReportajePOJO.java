package POJOs;

public class ReportajePOJO {
	
	private String id_reportaje;
	private String titulo;
	private String subtitulo;
	private String cuerpo;
	private int finalizado;
	
	public int getFinalizado() {
		return finalizado;
	}
	public void setFinalizado(int finalizado) {
		this.finalizado = finalizado;
	}
	public String getId_reportaje() {
		return id_reportaje;
	}
	public void setId_reportaje(String id_reportaje) {
		this.id_reportaje = id_reportaje;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getSubtitulo() {
		return subtitulo;
	}
	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}
	public String getCuerpo() {
		return cuerpo;
	}
	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}
	
	

}

