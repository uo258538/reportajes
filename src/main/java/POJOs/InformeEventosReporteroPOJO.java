package POJOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InformeEventosReporteroPOJO {
	
	private int id_evento;
	private String titulo;
	private String pais;
	private String region;
	private String diaInicio;
	private String diaFin;
	private String dieta_alimentacion;
	private String dieta_alojamiento;


}
