package POJOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class ReporteroPOJO {
	private String id_reportero;
	private String nombre;
	private String responsable;
	private String rol;
	
	public ReporteroPOJO() {}
	public ReporteroPOJO(String id_reportero, String nombre,String responsable, String rol) {
		this.id_reportero = id_reportero;
		this.nombre=nombre;
		this.responsable=responsable;
		this.rol = rol;
	}

}
