package POJOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class InformeReporterosPOJO {

	private int id_reportero;
	private String nombre;
	private String region;
	private int dieta_alimentacion;
	private int dieta_alojamiento;
}
