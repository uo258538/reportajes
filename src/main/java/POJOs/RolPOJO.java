package POJOs;

import lombok.Getter;
import lombok.Setter;
@Setter @Getter 
public class RolPOJO {
	private String id_rol;
	private String nombre;
	

	public RolPOJO() {}
	public RolPOJO(String id_rol, String nombre) {
			this.id_rol = id_rol;
			this.nombre = nombre;
		}
	
	public String getId_rol() {
		return id_rol;
	}
	public String getNombre() {
		return nombre;
	}

}
