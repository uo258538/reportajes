package POJOs;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class MultimediaPOJO {
	

	private String id_multimedia;
	private String tipo;
	private String ruta;
	private String id_reportaje;
	
	public MultimediaPOJO() {};
	
	public MultimediaPOJO(String id, String tipo, String ruta,String id_r) {
		this.id_multimedia=id;
		this.tipo=tipo;
		this.ruta=ruta;
		this.id_reportaje=id_r;
	}
}
