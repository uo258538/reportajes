package POJOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DietaPOJO {

	private int id;
	private String tipo;
	private int importe;
	
	public DietaPOJO() {
		
	}
	
	
}
