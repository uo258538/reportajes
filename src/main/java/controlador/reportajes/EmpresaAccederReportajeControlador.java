package controlador.reportajes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.table.TableModel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import util.reportajes.SwingUtil;
import POJOs.EventoDisplayPOJO;
import POJOs.ReportajePOJO;
import modelo.reportajes.modelo;
import vista.reportajes.EmpresaAccederReportajeView;

public class EmpresaAccederReportajeControlador {
	
	private modelo modelo;
	private EmpresaAccederReportajeView vista;
	
	
	public EmpresaAccederReportajeControlador(modelo modelo, EmpresaAccederReportajeView vista) {
		this.modelo=modelo;
		this.vista=vista;
		this.initView();
	}
	
	public void initController() {
		
		vista.getBtnAceptar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> this.getListaEventos(this.getIdEmpresa())));

		vista.getTableEventos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				
				SwingUtil.exceptionWrapper(() -> setReportaje());
			}
		});	
			
	}
	
	public void initView() {
		vista.getFrame().setVisible(true);
		this.getListaEventos("0");		
	}
	
	public void getListaEventos(String id_empresa) {
		List<EventoDisplayPOJO> eventos=modelo.getEventos(id_empresa);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(eventos, new String[] {"id_evento","diaInicio","descripcion","titulo"});
		vista.getTableEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableEventos());
	}
	
	public void setReportaje() {
		String id_evento=SwingUtil.getSelectedKey(vista.getTableEventos());
		//System.out.printf("Id evento: %s\n", id_evento);
		//System.out.printf("Aqui %s\n", id_reportaje);
		List<ReportajePOJO> reportajes=modelo.getReportaje(id_evento);
		vista.getTextFieldTitulo().setText(reportajes.get(0).getTitulo());
		vista.getTextFieldSubtitulo().setText(reportajes.get(0).getSubtitulo());
		vista.getTextFieldCuerpo().setText(reportajes.get(0).getCuerpo());
		
	}
	
	public String getIdEmpresa() {
		return vista.getTextFieldIdEmpresa().getText();
	}
	
	

}
