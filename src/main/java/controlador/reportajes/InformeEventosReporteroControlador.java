package controlador.reportajes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.TableModel;

import POJOs.EventoDisplayPOJO;
import POJOs.InformeEventosReporteroPOJO;
import POJOs.LugarPOJO;
import modelo.reportajes.modelo;
import util.reportajes.SwingUtil;
import util.reportajes.Util;
import vista.reportajes.InformeEventosReporteroView;
import vista.reportajes.ReporteroImporteDietaView;

public class InformeEventosReporteroControlador {

	private modelo modelo;
	private InformeEventosReporteroView vista;
	private String id_reportero;
	private List<InformeEventosReporteroPOJO> lista_eventos=new ArrayList<InformeEventosReporteroPOJO>();


	public InformeEventosReporteroControlador(modelo m, InformeEventosReporteroView v) {
		this.modelo = m;
		this.vista = v;
		this.initView();
	}

	private void initView() {
		vista.getFrame().setVisible(true);

	}

	public void initController() {
		vista.getBtnAceptar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> setIdReportero()));
		vista.getBtnAplicarFiltroFecha().addActionListener(e -> SwingUtil.exceptionWrapper(() -> setInforme()));
	}

	public void setIdReportero() {
		this.limpiarVentana();
		this.id_reportero=vista.getTextFieldIdReportero().getText();
		vista.getBtnAplicarFiltroFecha().setEnabled(!this.id_reportero.equals(""));
	}

	public void setInforme() {
		String filtroInicio = vista.getTextFieldFechaIni().getText();
		String filtroFin = vista.getTextFieldFechaFin().getText();
		Date fechaHoy = new Date();

		if(!(filtroInicio.equals("") && filtroFin.equals(""))) {
			
		
		if(filtroFin.equals("")) {
			filtroFin = "9999-01-01";
		}

		if(filtroInicio.equals("") ) {
			filtroInicio = "1900-01-01";
		}

		this.lista_eventos=modelo.getEventosReporteroFiltrado(this.id_reportero, filtroInicio, filtroFin);
		comprobarDieta();
		TableModel tmodel=SwingUtil.getTableModelFromPojos(this.lista_eventos, new String[] {
				"id_evento","titulo","pais","region","diaInicio","diaFin", "dieta_alimentacion","dieta_alojamiento"});
		vista.getTableEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableEventos());

		int total=0;
		int duracion=0;
		for(InformeEventosReporteroPOJO e: this.lista_eventos) {
			duracion = (int) ((Util.isoStringToDate(e.getDiaFin()).getTime()- Util.isoStringToDate(e.getDiaInicio()).getTime())/86400000);
			total+=(Integer.parseInt(e.getDieta_alimentacion()) + Integer.parseInt(e.getDieta_alojamiento()))*duracion;
			System.out.printf("Duracion: %d\n", duracion);

		}
		if(total<0)
			total=0;
		vista.getTextFieldTotalDietas().setText(String.valueOf(total)+ "�");
		}
	}

	public void comprobarDieta() {
		String regionReportero = modelo.getRegionReportero(this.id_reportero);
		for(InformeEventosReporteroPOJO e: this.lista_eventos) {
			if(e.getRegion().equals(regionReportero))
				e.setDieta_alojamiento("0");
		}
	}

	public void limpiarVentana() {
		vista.getTextFieldFechaIni().setText("");
		vista.getTextFieldFechaFin().setText("");
		vista.getBtnAplicarFiltroFecha().setEnabled(false);
		vista.getTextFieldTotalDietas().setText("");
		this.lista_eventos.removeAll(this.lista_eventos);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(this.lista_eventos, new String[] {
				"id_evento","titulo","pais","region","diaInicio","diaFin", "dieta_alimentacion","dieta_alojamiento"});
		vista.getTableEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableEventos());


	}

}
