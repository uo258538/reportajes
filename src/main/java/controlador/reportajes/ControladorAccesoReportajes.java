package controlador.reportajes;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import POJOs.EmpresaPOJO;
import POJOs.EquipoPOJO;
import POJOs.EventoDisplayPOJO;
import POJOs.EventoPOJO;
import POJOs.ReporteroPOJO;
import util.reportajes.Util;
import util.reportajes.Database;
import util.reportajes.SwingUtil;
import modelo.reportajes.modelo;
import vista.reportajes.AccesoReportajes_vista;
import vista.reportajes.AsignarRepEven_vista;
import vista.reportajes.OFREP_vista;

public class ControladorAccesoReportajes {
	private AccesoReportajes_vista v;
	private modelo model;
	private String lastSelectedKey="";

	String idEvento;
	String idEmpresa;
	
	public ControladorAccesoReportajes(modelo m, AccesoReportajes_vista v) {
		this.model = m;
		this.v = v;
		this.initView();
	}
	
	public void initController() {
		v.getTablaEventos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				
				SwingUtil.exceptionWrapper(() -> getListaEmpresasOfr());
			}
		});
		
		v.getBotonPermitirAcceso().addActionListener(e -> SwingUtil.exceptionWrapper(() -> PermitirAcceso()));
	}
	
	
	public void initView() {
		v.getFrame().setVisible(true);
		v.getFrame().setSize(800, 300);
		this.getListaEventos();		
	} 
	
	public void getListaEventos() {
		List<EventoPOJO> eventos=model.getEventosConReportajeTerminado_Tabla();
		TableModel tmodel=SwingUtil.getTableModelFromPojos(eventos, new String[] {"id_evento", "titulo"});
		v.getTablaEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(v.getTablaEventos());
	}
	
	public void getListaEmpresasOfr() {
		this.lastSelectedKey=SwingUtil.getSelectedKey(v.getTablaEventos());
		idEvento=String.valueOf(this.lastSelectedKey);
		idEvento=this.lastSelectedKey;
		List<EmpresaPOJO> eventos=model.getEmpresasAcepOfrePag_Tabla(idEvento);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(eventos, new String[] {"id_empresa", "nombre"});
		v.getTablaEmpresasOfrecimiento().setModel(tmodel);
		SwingUtil.autoAdjustColumns(v.getTablaEmpresasOfrecimiento());

	}
	
	public void PermitirAcceso() {
		this.lastSelectedKey=SwingUtil.getSelectedKey(v.getTablaEmpresasOfrecimiento());
		idEmpresa=String.valueOf(lastSelectedKey);
		model.PermitirAcceso(idEvento,idEmpresa);
		this.getListaEmpresasOfr();
	}
	
	
	
	
}

