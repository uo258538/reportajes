package controlador.reportajes;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import POJOs.EmpresaPOJO;
import POJOs.EventoDisplayPOJO;
import util.reportajes.Util;
import util.reportajes.Database;
import util.reportajes.SwingUtil;
import modelo.reportajes.modelo;
import POJOs.OfrecimientoPOJO;
import modelo.reportajes.OFREP_modelo;
import vista.reportajes.AnadirReportajeView;
import vista.reportajes.AccesoReportajes_vista;
import vista.reportajes.AsignarRepEven_vista;
import vista.reportajes.EmpComunicInformeReportajeView;
import vista.reportajes.EmpresaAccederReportajeView;
import vista.reportajes.GREP_vista;
import vista.reportajes.InformeEventosGestorView;
import vista.reportajes.InformeEventosReporteroView;
import vista.reportajes.OFREP_vista;
import vista.reportajes.ReporteroImporteDietaView;
import vista.reportajes.VE_vista;
import vista.reportajes.Menu_vista;


public class Controlador {
	private Menu_vista menu;
	private modelo model;
		
	public Controlador(modelo m,Menu_vista a) {
		this.model = m;
		this.menu=a;
		model.inicializar(true);
		this.initController();
		menu.getFrame().setVisible(true);
		menu.getFrame().setSize(800, 300);
		
	}
	
	//Crea el controlador para la ventana OFREP y la abre
	public void ventanaOFREP() {
		OFREP_Controler contr = new OFREP_Controler(model, new OFREP_vista());
		contr.initController();
		
	}
	
	//Crea el controlador para la ventana GREP y la abre
	public void ventanaGREP() {
		GREP_Controler contr = new GREP_Controler(model, new GREP_vista());
		contr.initController();
		}
	
	public void ventanaAsignarRepEven() {
		ControladorAsignarRepEven contr = new ControladorAsignarRepEven(model, new AsignarRepEven_vista());
		contr.initController();
		}
	public void ventanaAnadirReportajeView() {
		ReportajesControlador contr = new ReportajesControlador(model, new AnadirReportajeView());
		contr.initController();
		}
	public void ventanaEmpresaAccederReportajeView() {
		EmpresaAccederReportajeControlador contr = new EmpresaAccederReportajeControlador(model, new EmpresaAccederReportajeView());
		contr.initController();
		}
	
	

	public void ventanaVE() {
		VE_Controler contr = new VE_Controler(model, new VE_vista());
		contr.initController();
	}
		

	public void ventanaAccesoReportajes() {
		ControladorAccesoReportajes contr = new ControladorAccesoReportajes(model, new AccesoReportajes_vista());
		contr.initController();
		}
	
	public void ventanaReporteroImporteDietaView() {
		ReporteroImporteDietaControlador contr = new ReporteroImporteDietaControlador(model, new ReporteroImporteDietaView());
		contr.initController();
		}
	
	public void ventanaEmpComunicInformeReportajes() {
		EmpComunicInformeReportajes contr = new EmpComunicInformeReportajes(model, new EmpComunicInformeReportajeView());
		contr.initController();
		}
	
	public void ventanaInformeEventosReportero() {
		InformeEventosReporteroControlador contr = new InformeEventosReporteroControlador(model, new InformeEventosReporteroView());
		contr.initController();
		}
	
	public void ventanaInformeEventosGestor() {
		InformeEventosGestorControlador contr = new InformeEventosGestorControlador(model, new InformeEventosGestorView());
		contr.initController();
		}

	
	public void initController() {
		menu.getBtnDarAcceso().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaAccesoReportajes()));
		menu.getBtnAsigRep().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaAsignarRepEven()));
		menu.getBtnDsitribuirReportaje().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaOFREP()));
		menu.getBtnInforme().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaInformeEventosReportero()));
		menu.getBtnInformeDeReportajes().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaEmpComunicInformeReportajes()));
		menu.getBtnInformeDeUn().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaInformeEventosGestor()));
		menu.getBtnNVerRep().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaEmpresaAccederReportajeView()));
		menu.getBtnVerReporterosDe().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaVE()));
		menu.getGestRep().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaAnadirReportajeView()));
		menu.getImpDieta().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaReporteroImporteDietaView()));
		menu.getVerOfre().addActionListener(e -> SwingUtil.exceptionWrapper(() -> ventanaGREP()));
	}


}
