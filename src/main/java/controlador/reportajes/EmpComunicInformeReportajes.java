package controlador.reportajes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.TableModel;

import POJOs.EventoDisplayPOJO;
import POJOs.EventoPOJO;
import POJOs.InformeReportajePOJO;
import POJOs.LugarPOJO;
import POJOs.ReportajePOJO;
import modelo.reportajes.modelo;
import util.reportajes.SwingUtil;
import vista.reportajes.EmpComunicInformeReportajeView;
import vista.reportajes.ReporteroImporteDietaView;

public class EmpComunicInformeReportajes {

	private modelo modelo;
	private EmpComunicInformeReportajeView vista;
	private String id_empresa;
	private List<InformeReportajePOJO> lista_reportaje=new ArrayList<InformeReportajePOJO>();


	public EmpComunicInformeReportajes(modelo m, EmpComunicInformeReportajeView v) {
		this.modelo = m;
		this.vista = v;
		this.initView();
	}

	private void initView() {
		vista.getFrame().setVisible(true);

	}

	public void initController() {
		vista.getBtnAceptar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> setIdEmpresa()));
		vista.getBtnAplicarFiltroFecha().addActionListener(e -> SwingUtil.exceptionWrapper(() -> getListaReportajeFiltro()));

	}

	public void setIdEmpresa() {
		this.limpiarVista();
		this.getIdEmpresa();
		vista.getBtnAplicarFiltroFecha().setEnabled(!this.id_empresa.equals(""));

	}

	public void limpiarVista() {
		vista.getTextFieldFiltroFechaIni().setText("");
		vista.getTextFieldFiltroFechaF().setText("");
		vista.getTextFieldPrecioTotal().setText("");
		this.lista_reportaje.removeAll(this.lista_reportaje);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_reportaje, new String[] {
				"id_reportaje","titulo","nombre","titulo_evento","pais","region","diaInicio","diaFin", "precio"});
		vista.getTableReportajes().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableReportajes());


	}
	public void getListaReportajeFiltro() {
		vista.getTextFieldPrecioTotal().setText("");
		vista.getBtnAplicarFiltroFecha().setEnabled(true);
		this.getIdEmpresa();
		String filtroInicio = vista.getTextFieldFiltroFechaIni().getText();
		String filtroFin = vista.getTextFieldFiltroFechaF().getText();

		if(!(filtroInicio.equals("") && filtroFin.equals(""))) {


			if(filtroFin.equals("")) {
				filtroFin = "9999-01-01";
			}

			if(filtroInicio.equals("") ) {
				filtroInicio = "1900-01-01";
			}
			
			
			
			this.lista_reportaje=modelo.getReportajesConAccesoFiltradoFecha(this.id_empresa, filtroInicio, filtroFin);
			TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_reportaje, new String[] {
					"id_reportaje","titulo","nombre","titulo_evento","pais","region","diaInicio","diaFin", "precio"});
			vista.getTableReportajes().setModel(tmodel);
			SwingUtil.autoAdjustColumns(vista.getTableReportajes());

			int total=0;
			if(this.lista_reportaje.size()!=0) {
				for(InformeReportajePOJO e : this.lista_reportaje) {
					total+=e.getPrecio();
					System.out.printf("Precio: %d\n", e.getPrecio());
				}
			}
			vista.getTextFieldPrecioTotal().setText(String.valueOf(total)+"�");

		}
	}

	public void getIdEmpresa() {
		this.id_empresa=vista.getTextFieldEmpComunic().getText();
	}

}
