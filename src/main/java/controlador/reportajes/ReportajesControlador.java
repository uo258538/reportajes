package controlador.reportajes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import util.reportajes.SwingUtil;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import POJOs.EquipoPOJO;
import POJOs.EventoDisplayPOJO;
import POJOs.EventoPOJO;
import POJOs.MultimediaPOJO;
import POJOs.ReportajePOJO;
import modelo.reportajes.modelo;
import vista.reportajes.AccesoReportajes_vista;
import vista.reportajes.AnadirReportajeView;

/**
 * @author Usuario
 *
 */
public class ReportajesControlador {

	private AnadirReportajeView vista;
	private modelo modelo;
	private String id_evento="";
	private int id_reportaje=0;
	private String id_reportero="0";
	private List<EventoDisplayPOJO> lista_eventos=new ArrayList<EventoDisplayPOJO>();
	private List<EventoDisplayPOJO> lista_eventos_responsable=new ArrayList<EventoDisplayPOJO>();
	private List<MultimediaPOJO> lista_multimedia=new ArrayList<MultimediaPOJO>();
	private String last_selected_key_multimedia="";
	private boolean revisado = false;

	//Para la parte de comentarios
	private List<EventoDisplayPOJO> lista_eventos_reportajes; //Lista de eventos cuyos reportajes no finalizados
	private String last_selected_key_eventos_reportaje="";
	private List<EventoDisplayPOJO> lista_eventos_reportaje_responsable=new ArrayList<EventoDisplayPOJO>();
	private List<EquipoPOJO> comentarios = new ArrayList<EquipoPOJO>();
	private List<MultimediaPOJO> lista_multimedia_backup=new ArrayList<MultimediaPOJO>();





	public ReportajesControlador(modelo m, AnadirReportajeView v) {
		this.modelo=m;
		this.vista=v;
		this.initView();
	}


	public void initController() {
		vista.getBtnAceptar().addActionListener(e-> SwingUtil.exceptionWrapper(() ->getListaEventos()));
		vista.getBtnAnadirReportaje().addActionListener(e -> SwingUtil.exceptionWrapper(() -> a�adirReportaje()));
		vista.getBtnAnadirMultimedia().addActionListener(e-> SwingUtil.exceptionWrapper(() ->getListaMultimedia()));
		vista.getBtnEliminar().addActionListener(e-> SwingUtil.exceptionWrapper(() ->eliminarMultimedia()));
		vista.getBtnGuardarComentario().addActionListener(e-> SwingUtil.exceptionWrapper(() ->guardarComentario()));
		vista.getBtnActualizarReportaje().addActionListener(e-> SwingUtil.exceptionWrapper(() ->ActualizarReportaje()));
		
		vista.getBtnNewButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modelo.terminarReportaje(last_selected_key_eventos_reportaje);
				getListaEventosConReportajes();
				JOptionPane.showMessageDialog(vista.getFrame(), "Se ha terminado el reportaje");
				limpiarVentana();
			}
		});


		vista.getTableEventos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SwingUtil.exceptionWrapper(() -> MuestraBtnReportaje());
	



			}

		});
		vista.getTableMultimedia().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SwingUtil.exceptionWrapper(() -> muestraBtnEliminar());



			}

		});

		vista.getTableEventosConReportajes().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				revisado = todosRevisaron();
				SwingUtil.exceptionWrapper(() -> muestraBtnComentario());
				


			}

		});

	}

	/*public void lanza() {
		Controlador c = new Controlador(new modelo());
		c.ventanaAccesoReportajes();
	}*/


	public void initView() {
		this.getListaEventos();
		vista.getFrame().setVisible(true);

	}

	/**
	 *Obtiene el id del reportero en sesi�n(el que se pone en el textfield) y lo almacena en el atributo id_reportero.
	 */
	public String getIdReportero() {
		this.id_reportero=(vista.getTextFieldIdReportero().getText());
		return (id_reportero);
	}

	/**
	 * Si se ha seleccionado un evento permite a�adir un reportaje (boton Anadir Reportaje enable)
	 */
	public void MuestraBtnReportaje() {
		//Limpiamos todos los campos y ponemos botones como en el estado inicial
		this.limpiarVentana();

		//Utiliza la ultimo valor de la clave (que se reiniciara si ya no existe en la tabla)
		this.id_evento=(String) vista.getTableEventos().getModel().getValueAt(vista.getTableEventos().getSelectedRow(), 0);


		//Si hay clave para seleccionar en la tabla muestra el boton si no lo esconde

		//No hay nada seleccionado
		if ("".equals(this.id_evento)) { 
			//Boton para a�adir reportaje y para a�adir multimedia desactivados
			vista.getBtnAnadirReportaje().setEnabled(false);
			vista.getBtnAnadirMultimedia().setEnabled(false);
		} 

		//Si hay clave seleccionada
		else {
			//pone Enable el boton de a�adir reportaje y multimedia si es responsable
			if(responsable()) {
				vista.getBtnAnadirReportaje().setEnabled(true);
				vista.getBtnAnadirMultimedia().setEnabled(true);
				
			}
		}
	}




	/**
	 * comprueba si el reporteroen sesi�n es responsable del evento selecionado
	 * @return true si es responsable. False en otro caso.
	 */
	private boolean responsable() {
		for(EventoDisplayPOJO aux: this.lista_eventos_responsable) {
			if(aux.getId_evento().equals(this.id_evento)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * comprueba si un reportero es responsable de un reportaje de el evento que se le pasa como parametro
	 * @param id_evento
	 * @return true si es responable
	 */
	private boolean responsableReportaje(String id_evento_reportaje) {
		for(EventoDisplayPOJO aux: this.lista_eventos_reportaje_responsable) {
			if(aux.getId_evento().equals(id_evento_reportaje)) {
				return true;
			}
		}
		return false;
	}

	//Comprueba que todos los reporteros han revisado el reportaje
	private boolean todosRevisaron() {
		boolean rev = true;
		List<EquipoPOJO> equipos = modelo.obtieneReporterosEquipo(last_selected_key_eventos_reportaje);
		System.out.print(last_selected_key_eventos_reportaje);
		for (EquipoPOJO equipo : equipos ) {
			if (equipo.getRevisado() == 0) {
				return false;
			}
			System.out.printf("%s lo ha revisado\n",equipo.getId_reportero());
			
		}
		
		return rev;
		
	}
	
	
	/**
	 * comprueba si existe un comentario en el POJO equipo 
	 * @param POJOequipo del que queremos comprobar si tiene comentario
	 * @return true si hay comentario. False en otro caso
	 */
	public boolean existeComentario(EquipoPOJO e) {
		if(e==null)
			return false;
		if(e.getComentario()==null)
			return false;
		return true;

	}


	/**
	 * Obtiene los eventos que no tengan reportaje
	 */
	public void getListaEventos() {
		//No deber�a ser necesario limpiar la ventana la primera vez pero  
		//si se cambia de reportero si es necesario
		this.limpiarVentana();

		this.id_reportero=this.getIdReportero();
		this.lista_eventos=modelo.getEventosSinReportaje(this.id_reportero);
		this.lista_eventos_responsable=this.modelo.getEventosSinReportajeEsResponsable(this.id_reportero);

		TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_eventos, new String[] {"id_evento","titulo","diaInicio","diaFin","descripcion"});
		vista.getTableEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableEventos());

		//Pitar tambi�n los eventos con reportaje de ese reportero
		this.getListaEventosConReportajes();


	}


	/**
	 * Pinta en la tabla los eventos con reportaje no finalizo del reportero en sesi�n
	 */
	private void getListaEventosConReportajes() {
		this.lista_eventos_reportajes=modelo.getEventosConReportajesNoFinalizados(this.id_reportero);
		//this.lista_eventos_responsable=this.modelo.getEventosSinReportajeEsResponsable(this.id_reportero);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_eventos_reportajes, new String[] {"id_evento","titulo","diaInicio","diaFin","descripcion"});
		vista.getTableEventosConReportajes().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableEventosConReportajes());

	}


	/**
	 * A�ade el reportaje introducido en los textfields (como no finalizado, pendiente de revisi�n)
	 */
	public void a�adirReportaje() {

		ReportajePOJO reportaje= cogerReportaje();
		int id_evento=Integer.parseInt(this.id_evento);

		this.id_reportaje=this.getIdReportaje();
		modelo.insertaReportaje(id_reportaje, reportaje.getTitulo(),reportaje.getSubtitulo(), 
				reportaje.getCuerpo(), reportaje.getFinalizado(), id_evento);
		preparaMultimedias(id_reportaje);
		insertaMultimedia(this.lista_multimedia);

		//Di�logo para ver que se hizo algo
		JOptionPane.showMessageDialog( null, "Reportaje a�adido con �xito","Reportaje A�adido",JOptionPane.PLAIN_MESSAGE);

		this.limpiarVentana();

		//Para actualizar la lista
		this.getListaEventos();
		id_reportaje++;
	}

	/**
	 * Obtiene el id del reportaje al que se asocian las im�genes o v�deos y el cual no se conoce hasta que el reportaje es a�adido
	 * @param id_reportaje2
	 */
	private void preparaMultimedias(int id_reportaje2) {
		for(MultimediaPOJO m: this.lista_multimedia) {
			m.setId_reportaje(Integer.toString(id_reportaje2));
		}
	}


	/**
	 * obtiene el id del reportaje a insertar (el id del �ltimo reportaje +1)
	 * @return id del reportaje a insertar
	 */
	public int getIdReportaje() {
		int id=0;
		List<ReportajePOJO> reportajes=modelo.listaReportajes();
		id=Integer.parseInt(reportajes.get(reportajes.size()-1).getId_reportaje());
		return id+1;
	}

	/**
	 * Extrae el reportaje de los textfield
	 * @return el reportaje
	 */
	public ReportajePOJO cogerReportaje() {
		ReportajePOJO reportaje = new ReportajePOJO();
		reportaje.setTitulo(vista.getTextPaneCuerpo().getText());
		reportaje.setSubtitulo(vista.getTextFieldSubtitulo().getText());
		reportaje.setCuerpo(vista.getTextPaneCuerpo().getText());
		reportaje.setFinalizado(0);
		return reportaje;
	}

	/**
	 * Restaura el valor de los elementos de la ventana, setEnabled de botones y
	 * setText de textField y deselecciona celdas de la tabla de eventos con reportaje no finalizado...
	 * Es llamado cuando se pulsa sobre un elemento de la tabala de eventos sin reportaje
	 */
	public void limpiarVentana() {
		vista.getTextFieldTitulo().setText("");
		vista.getTextFieldTitulo().setEditable(true);

		vista.getTextFieldSubtitulo().setText("");
		vista.getTextFieldSubtitulo().setEditable(true);

		vista.getTextPaneCuerpo().setText("");
		vista.getTextPaneCuerpo().setEditable(true);

		vista.getBtnAnadirReportaje().setEnabled(false);

		vista.getTextFieldRuta().setText("");
		this.lista_multimedia.removeAll(this.lista_multimedia);
		this.lista_multimedia_backup.removeAll(this.lista_multimedia_backup);
		this.getListaMultimediaEliminar();
		vista.getBtnEliminar().setEnabled(false);
		vista.getTableMultimedia().setEnabled(true);

		vista.getBtnGuardarComentario().setEnabled(false);
		vista.getTextPaneComentario().setText("");
		vista.getChckbxRevisado().setSelected(false);
		vista.getChckbxRevisado().setEnabled(false);
		this.limpiarTablaComentario();
		vista.getBtnActualizarReportaje().setEnabled(false);
		vista.getBtnNewButton().setEnabled(false);



		//Seleccionamos todas porque si no no se puede hacer clear en el caso de que no haya ninguna seleccionada
		vista.getTableEventosConReportajes().selectAll();
		vista.getTableEventosConReportajes().clearSelection();


	}

	/**
	 * Restaura el valor de los elementos de la ventana, setEnabled de botones y
	 * setText de textField y deselecciona celdas de la tabla de eventos
	 * Es llamado cuando se pulsa sobre un elemento de la tabala de eventos con reportaje
	 */
	public void limpiarVentanaTablaEventosReportaje() {
		//Limpiamos los txtField del reportaje
		vista.getTextFieldTitulo().setText("");
		vista.getTextFieldTitulo().setEditable(true);

		vista.getTextFieldSubtitulo().setText("");
		vista.getTextFieldSubtitulo().setEditable(true);

		vista.getTextPaneCuerpo().setText("");
		vista.getTextPaneCuerpo().setEditable(true);

		vista.getBtnAnadirReportaje().setEnabled(false);

		//Limpiamos la tabla de multimedia
		vista.getTextFieldRuta().setText("");
		this.lista_multimedia.removeAll(this.lista_multimedia);
		this.lista_multimedia_backup.removeAll(this.lista_multimedia_backup);

		this.getListaMultimediaEliminar();
		vista.getBtnEliminar().setEnabled(false);
		vista.getBtnAnadirMultimedia().setEnabled(false);
		vista.getTableMultimedia().setEnabled(false);

		//Limpiamos la parte de los comentarios (no har�a falta pero por seguridad)
		vista.getBtnGuardarComentario().setEnabled(false);
		vista.getChckbxRevisado().setSelected(false);
		vista.getTextPaneComentario().setText("");
		this.limpiarTablaComentario();
		vista.getBtnActualizarReportaje().setEnabled(false);


		//Eliminamos selecci�n de la tabla de eventos sin reportaje
		//Seleccionamos todas porque si no no se puede hacer clear en el caso de que no haya ninguna seleccionada
		vista.getTableEventos().selectAll();
		vista.getTableEventos().clearSelection();


	}
	
	

	/**
	 * Pinta en la tabla la lista de im�genes que ha a�adido el reportero
	 */
	public void getListaMultimedia() {
		//Cogemos la ruta introducida por el reportero
		String ruta=vista.getTextFieldRuta().getText();

		//Obtenemos el multimedia de la bbdd
		MultimediaPOJO m = this.cogerMultimedia(ruta);

		//Comprobamos que esa imagen existe o que no est� ya en uso
		if(m==null)
			JOptionPane.showMessageDialog(null, "El contenido multimedia no existe", 
					"La ruta introducida no es v�lida", JOptionPane.WARNING_MESSAGE);	
		else {
			//Si no est� ya a�adida la a�ade a la lista de multimedia
			if(!contiene(this.lista_multimedia,m)) {
				this.lista_multimedia.add(m);
				if(!contiene(this.lista_multimedia_backup,m))
					this.lista_multimedia_backup.add(m);
			}
			
			TableModel tmodel=SwingUtil.getTableModelFromPojos(this.lista_multimedia, new String[] {"id_multimedia","tipo","ruta"});
			vista.getTableMultimedia().setModel(tmodel);
			SwingUtil.autoAdjustColumns(vista.getTableMultimedia());

		}
	}

	/**
	 * Igual que el anterior pero se usa cuando se elimina una imagen porque el otro no es v�lido
	 */
	public void getListaMultimediaEliminar() {
		TableModel tmodel=SwingUtil.getTableModelFromPojos(this.lista_multimedia, new String[] {"id_multimedia","tipo","ruta"});
		vista.getTableMultimedia().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableMultimedia());

	}
	/**
	 * Comprueba si un contenido multimedia est� ya elegido
	 * @param multimedia
	 * @return true si ya lo contiene. false en otro caso
	 */

	public boolean contiene(List<MultimediaPOJO> l,MultimediaPOJO m) {
		for(MultimediaPOJO e: l) {
			if(e.getId_multimedia().equals(m.getId_multimedia()))
				return true;
		}
		return false;
	}


	/**
	 * Crea un POJO con la imagen o video introducido en la ventana
	 * @return la imagen
	 */
	public MultimediaPOJO cogerMultimedia(String ruta) {
		List<MultimediaPOJO> m = new ArrayList<MultimediaPOJO>();
		m=modelo.getMultimedia(ruta);

		if(!m.isEmpty())
			return m.get(0);

		return null;
	}


	/**
	 * Si se selecciona un multimedia de la lista de multimedias activa el bot�n
	 */
	public void muestraBtnEliminar() {
		//Utiliza la ultimo valor de la clave (que se reiniciara si ya no existe en la tabla)
		if(vista.getTableMultimedia().isEnabled()) {
			this.last_selected_key_multimedia= (String) vista.getTableMultimedia().getModel().getValueAt(vista.getTableMultimedia().getSelectedRow(), 0);
			//Si hay clave para seleccionar en la tabla muestra el boton si no lo esconde
			if ("".equals(last_selected_key_multimedia)) { 
				vista.getBtnEliminar().setEnabled(false);

			}
			else 
				vista.getBtnEliminar().setEnabled(true);	

		}

	}

	/** 
	 * Permite hacer comentarios sobre el evento con reportaje seleccionado.
	 */
	public void muestraBtnComentario() {
		//Deja ventana en estado inicial
		this.limpiarVentanaTablaEventosReportaje();
		//Obtiene el elmento seleccionado
		this.last_selected_key_eventos_reportaje=(String) vista.getTableEventosConReportajes().getModel().getValueAt(vista.getTableEventosConReportajes().getSelectedRow(),0);
		this.id_reportaje=Integer.valueOf(modelo.getIdReportajeEvento(this.last_selected_key_eventos_reportaje));
		//Obitne el reportaje del evento seleccionado
		List<ReportajePOJO> lista_reportajes= modelo.getReportaje(this.last_selected_key_eventos_reportaje);

		ReportajePOJO r=lista_reportajes.get(0);
		//Lo introduce en la ventana
		colocaReportaje(r);

		//Si hay clave para seleccionar en la tabla muestra el boton si no lo esconde

		if ("".equals(last_selected_key_eventos_reportaje)) { 
			vista.getBtnGuardarComentario().setEnabled(false);
			vista.getChckbxRevisado().setEnabled(false);


		}

		else { //Hay un evento con reportaje seleccionado

			lista_eventos_reportaje_responsable=modelo.getEventosConReportajeEsResponsable(this.id_reportero);
			if(this.responsableReportaje(this.last_selected_key_eventos_reportaje)) {

				//Miramos si hay comentarios de otros usuarios
				this.comentarios=modelo.getComentarios(this.last_selected_key_eventos_reportaje);
				preparaListaComentarios();
				TableModel tmodel=SwingUtil.getTableModelFromPojos(comentarios, new String[] {"id_reportero","comentario","revisado"});
				vista.getTableComentarios().setModel(tmodel);
				SwingUtil.autoAdjustColumns(vista.getTableMultimedia());


				//Miramos si ese reportaje tiene un comentario ya hacho por ese reportero
				EquipoPOJO e=modelo.getEquipoComentario(this.last_selected_key_eventos_reportaje,this.id_reportero);
				if(existeComentario(e)) {
					//EquipoPOJO e=modelo.getEquipoComentario(this.last_selected_key_eventos_reportaje,this.id_reportero);
					vista.getTextPaneComentario().setText(e.getComentario());
					//Si ya est� finalizado no se puede editar
					if(e.getRevisado ()==1) {
						vista.getChckbxRevisado().setSelected(true);
						vista.getChckbxRevisado().setEnabled(false);
						vista.getTextPaneComentario().setEnabled(false);
						vista.getBtnGuardarComentario().setEnabled(false);

					}
					else {
						vista.getChckbxRevisado().setSelected(false);
						vista.getChckbxRevisado().setEnabled(true);
						vista.getTextPaneComentario().setEnabled(true);
						vista.getBtnGuardarComentario().setEnabled(true);

					}
				}

				//Si no est� finalizado el comentario se puede editar y finalizar
				else {
					vista.getChckbxRevisado().setSelected(false);
					vista.getChckbxRevisado().setEnabled(true);
					vista.getTextPaneComentario().setEnabled(true);
					vista.getBtnGuardarComentario().setEnabled(true);
					
				}
				
				editarReportaje();
				
				if (this.revisado) {
					vista.getBtnNewButton().setEnabled(true);
					}


			}
			else {//Si no es responsable puede hacer lo mismo menos la parte de visualizar comentarios

				//Miramos si ese reportaje tiene un comentario ya hacho por ese reportero
				EquipoPOJO e=modelo.getEquipoComentario(this.last_selected_key_eventos_reportaje,this.id_reportero);
				if(existeComentario(e)) {
					//EquipoPOJO e=modelo.getEquipoComentario(this.last_selected_key_eventos_reportaje,this.id_reportero);
					vista.getTextPaneComentario().setText(e.getComentario());
					//Si ya est� finalizado no se puede editar
					if(e.getRevisado ()==1) {
						vista.getChckbxRevisado().setSelected(true);
						vista.getChckbxRevisado().setEnabled(false);
						vista.getTextPaneComentario().setEnabled(false);
						vista.getBtnGuardarComentario().setEnabled(false);

					}
					else {
						vista.getChckbxRevisado().setSelected(false);
						vista.getChckbxRevisado().setEnabled(true);
						vista.getTextPaneComentario().setEnabled(true);
						vista.getBtnGuardarComentario().setEnabled(true);

					}
				}

				//Si no est� finalizado se puede editar y finalizar
				else {
					vista.getChckbxRevisado().setSelected(false);
					vista.getChckbxRevisado().setEnabled(true);
					vista.getTextPaneComentario().setEnabled(true);
					vista.getBtnGuardarComentario().setEnabled(true);

				}


			}
		}
	}

	/**
	 * Permite editar los textfield y multimedia de un reportaje que se ha cargado.
	 */
	private void editarReportaje() {

		vista.getTextFieldTitulo().setEditable(true);
		vista.getTextFieldSubtitulo().setEditable(true);
		vista.getTextPaneCuerpo().setEditable(true);
		vista.getBtnAnadirMultimedia().setEnabled(true);
		vista.getBtnEliminar().setEnabled(true);
		vista.getBtnActualizarReportaje().setEnabled(true);
		vista.getTableMultimedia().setEnabled(true);



	}


	/**
	 * Elimina de la lista de comentarios el comentario propio si es que existe
	 * @param comentarios
	 */
	private void preparaListaComentarios() {
		if(comentarios.size()>0) {
			List<EquipoPOJO> aux =new ArrayList<EquipoPOJO>();
			aux.addAll(comentarios);
			for(EquipoPOJO e: aux) {
				if(e.getId_reportero().equals(this.id_reportero))
					comentarios.remove(e);
			}
		}
	}


	/**
	 * Introduce el reportaje en la ventana y el contenido multimedia asociado y si hay comentarios los muestra
	 * @param reportaje
	 */
	private void colocaReportaje(ReportajePOJO reportaje) {
		//Colocamos el reportaje
		vista.getTextFieldTitulo().setEditable(false);
		vista.getTextFieldTitulo().setText(reportaje.getTitulo());
		vista.getTextFieldSubtitulo().setEditable(false);
		vista.getTextFieldSubtitulo().setText(reportaje.getSubtitulo());
		vista.getTextPaneCuerpo().setEditable(false);
		vista.getTextPaneCuerpo().setText(reportaje.getCuerpo());

		//Obtenemos los multimedia asociados y los pintamos
		this.lista_multimedia = modelo.getMultimediaConIdEvento(this.last_selected_key_eventos_reportaje);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_multimedia, new String[] {"id_multimedia","tipo","ruta"});
		vista.getTableMultimedia().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableMultimedia());

	} 


	/**
	 * Elimina una multimedia de la lista de multimedia
	 */
	public void eliminarMultimedia() {
		int i=Integer.parseInt(this.last_selected_key_multimedia);
		List<MultimediaPOJO> aux=new ArrayList<MultimediaPOJO>();
		aux.addAll(this.lista_multimedia);
		for(MultimediaPOJO m: aux) {
			//Si es igual que el elemento seleccionado lo borramos
			if(i==Integer.parseInt(m.getId_multimedia())) {
				if(!contiene(this.lista_multimedia_backup,m))
					this.lista_multimedia_backup.add(m);
				this.lista_multimedia.remove(m);
			}

		}
		//Volver a pintar
		this.getListaMultimediaEliminar();
		//Volvemos a desactivar el boton de eliminado
		vista.getBtnEliminar().setEnabled(false);
	}


	/**
	 * Modifica a que reportaje se une los contenidos multimedi elegidos
	 * Necesita la lista de imagenes 
	 * @param multimedia
	 */
	public void insertaMultimedia(List<MultimediaPOJO> multimedia) {
		List<MultimediaPOJO> l= modelo.getIdsMultimedia();
		int i=Integer.parseInt(l.get(l.size()-1).getId_multimedia());
		i++;
		for(MultimediaPOJO m: multimedia) {
			if(m.getId_multimedia()!=null) {
				m.setId_multimedia(String.valueOf(i));
				i++;
			}
		}
		modelo.insertaMultimedia(multimedia);

	}

	/**
	 * Guarda en la bbdd el comentario realizado
	 */
	public void guardarComentario() {
		EquipoPOJO e =new EquipoPOJO();
		e.setComentario(vista.getTextPaneComentario().getText());
		int revisado=0;
		if(vista.getChckbxRevisado().isSelected())
			revisado=1;
		e.setRevisado(revisado);
		modelo.guardaComentario(e,this.last_selected_key_eventos_reportaje,this.id_reportero);

		JOptionPane.showMessageDialog(null, "Comentario Guardado", 
				"El comentario ha sido guardado con �xito", JOptionPane.PLAIN_MESSAGE);	
		this.getListaEventos();
		this.limpiarVentana();
	}


	/**
	 * Elimina los elementos de la tabla de comentarios
	 */
	public void limpiarTablaComentario(){
		comentarios.removeAll(comentarios);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(comentarios, new String[] {"id_reportero","comentario","revisado"});
		vista.getTableComentarios().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableMultimedia());
	}

	/**
	 * Introduce los nuevos valores de los campos del reportaje y de la lista de multimedia
	 */
	public void ActualizarReportaje() {
		
		//Tituo subtitulo y cuerpo
		ReportajePOJO reportaje = new ReportajePOJO();
		String id_rep=modelo.getIdReportajeEvento(this.last_selected_key_eventos_reportaje);
		reportaje.setId_reportaje(id_rep);
		reportaje.setTitulo(vista.getTextFieldTitulo().getText());
		reportaje.setSubtitulo(vista.getTextFieldSubtitulo().getText());
		reportaje.setCuerpo(vista.getTextPaneCuerpo().getText());

		//backup tiene las que hab�a antes de realizar niguna acci�n
		//lista_multimedia tiene la lista actual
		for(MultimediaPOJO m:this.lista_multimedia_backup) {
			if(!contiene(this.lista_multimedia,m)) {
				//Pone id_reportaje a null
				modelo.actulizarMultimedia(m);
			}
			else {
				
				this.id_reportaje=Integer.parseInt(modelo.getIdReportajeEvento(this.last_selected_key_eventos_reportaje));
				//Actualizamos el id_reportaje de la imagen o video
				modelo.actulizarMultimediaNueva(m,String.valueOf(this.id_reportaje));
			}

		}

		//Actualizamos el contenido del reportaje en la bbdd
		modelo.actualizaReportaje(reportaje);
		JOptionPane.showMessageDialog( null, "Reportaje actulizado con �xito","Reportaje Actualizado",JOptionPane.PLAIN_MESSAGE);
		this.limpiarVentana();

	}


}
