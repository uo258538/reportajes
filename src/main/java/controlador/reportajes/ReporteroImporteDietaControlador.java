package controlador.reportajes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.TableModel;

import POJOs.DietaPOJO;
import POJOs.EventoDisplayPOJO;
import POJOs.LugarPOJO;
import modelo.reportajes.modelo;
import util.reportajes.SwingUtil;
import vista.reportajes.AccesoReportajes_vista;
import vista.reportajes.ReporteroImporteDietaView;

public class ReporteroImporteDietaControlador {

	private modelo modelo;
	private ReporteroImporteDietaView vista;
	private String id_reportero;
	private String id_evento;
	private List<EventoDisplayPOJO> lista_eventos=new ArrayList<EventoDisplayPOJO>();


	public ReporteroImporteDietaControlador(modelo m, ReporteroImporteDietaView v) {
		this.modelo = m;
		this.vista = v;
		this.initView();
	}

	private void initView() {
		vista.getFrame().setVisible(true);

	}

	public void initController() {
		vista.getTableEventos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {

				SwingUtil.exceptionWrapper(() -> getDieta());
			}
		});

		vista.getBtnAceptar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> getListaEventos()));
	}


	public void getListaEventos() {

		this.id_reportero=this.getIdReportero();
		this.lista_eventos=modelo.getEventosReportero(this.id_reportero);

		TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_eventos, new String[] {"id_evento","titulo","diaInicio","diaFin","descripcion"});
		vista.getTableEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableEventos());
	}

	public String getIdReportero() {

		return vista.getTextFieldIdReportero().getText();
	}

	public void getDieta() {

		vista.getTextFieldImporte().setText("");

		//Utiliza la ultimo valor de la clave (que se reiniciara si ya no existe en la tabla)
		this.id_evento=(String) vista.getTableEventos().getModel().getValueAt(vista.getTableEventos().getSelectedRow(), 0);


		//Si hay clave para seleccionar en la tabla muestra el boton si no lo esconde

		//No hay nada seleccionado
		if (!"".equals(this.id_evento)) { 
			int importeTotal=0;

			//Obtenemos la duración del evento seleccionado
			String diaIni= (String) vista.getTableEventos().getModel().getValueAt(vista.getTableEventos().getSelectedRow(), 2);
			String diaFin = (String) vista.getTableEventos().getModel().getValueAt(vista.getTableEventos().getSelectedRow(), 3);

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaInicial=new Date();
			Date fechaFinal = new Date();

			try {
				fechaInicial = dateFormat.parse(diaIni);
				fechaFinal=dateFormat.parse(diaFin);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			double duracion = (double) ((fechaFinal.getTime()-fechaInicial.getTime())/86400000);
			//System.out.printf("diaINI: %s \n",fechaFinal.getTime());
			//System.out.printf("diaFIN: %s \n",fechaInicial.getTime());
			System.out.printf("duracion: %f\n",duracion);


			LugarPOJO lugar = modelo.getLugar(Integer.parseInt(this.id_evento));
			if(lugar!=null) {
				//Obtenemos la dieta por alimentación del evento
				System.out.printf("Diesta alimentacion: %s\n", lugar.getDieta_alimentacion());
				importeTotal+=lugar.getDieta_alimentacion()*duracion;

				String reporteroRegion = modelo.getRegionReportero(this.id_reportero);

				//Miramos si el reportero vive en la misma región
				if(!reporteroRegion.equals(lugar.getRegion())) {
					System.out.printf("Diesta alojamiento: %s\n", lugar.getDieta_alojamiento());
					importeTotal+=lugar.getDieta_alojamiento()*duracion;
				}
				vista.getTextFieldImporte().setText(String.valueOf(importeTotal)+"€");
			}
		}
	}



}
