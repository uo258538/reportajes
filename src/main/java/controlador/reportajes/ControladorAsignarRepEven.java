package controlador.reportajes;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import POJOs.EmpresaPOJO;
import POJOs.EquipoPOJO;
import POJOs.EventoDisplayPOJO;
import POJOs.EventoPOJO;
import POJOs.LugarPOJO;
import POJOs.ReporteroPOJO;
import POJOs.RolPOJO;
import util.reportajes.Util;
import util.reportajes.Database;
import util.reportajes.SwingUtil;
import modelo.reportajes.modelo;
import vista.reportajes.AsignarRepEven_vista;
import vista.reportajes.OFREP_vista;

public class ControladorAsignarRepEven {
	private AsignarRepEven_vista v1;
	private modelo model;
	private String lastSelectedKey="";
	private String selectedRole = "base"; //Valor por defecto

	String idEvento;
	String idReportero;
	String nombreReportero;
	String fechaInicio;
	String fechaFin;
	String idResponsable;
	String Region;
	int NumDiaInicio;
	int NumDiaFin;
	int TipoContrato;
	
	public ControladorAsignarRepEven(modelo m, AsignarRepEven_vista v) {
		this.model = m;
		this.v1 = v;
		model.inicializar(false);
		this.initView();
	}
	
	public void initController() {
		
		v1.getBotonFinalizar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Finalizar();
			}
		});
		
		v1.getComboBoxTipo().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 selectedRole = (String)v1.getComboBoxTipo().getSelectedItem();
				 getReporterosRol();
			}
		});
		
		v1.getComboBoxMostrar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 getReporterosRol();
			}
		});
		
		v1.getTablaEventos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				
				SwingUtil.exceptionWrapper(() -> mostrar());
			}
		});	
				
		v1.getBotonAnadir().addActionListener(e -> SwingUtil.exceptionWrapper(() -> Anadir()));
	
		v1.getBotonMarcarResponsable().addActionListener(e -> SwingUtil.exceptionWrapper(() -> MarcarRes()));
	}
	
	
	public void initView() {
		v1.getFrame().setVisible(true);
		v1.getFrame().setSize(600, 600);
		this.getListaEventos();
		this.getRoles();
	} 
	
	public void mostrar() {
		this.getListaRepEnEquipo();
		this.getReporterosRol();
		v1.getComboBoxTipo().setEnabled(true);
		v1.getComboBoxMostrar().setEnabled(true);
		v1.getBotonFinalizar().setEnabled(true);
		
	}
	
	public void getReporterosRol() {
		this.lastSelectedKey=SwingUtil.getSelectedKey(v1.getTablaEventos());
		this.fechaInicio=(String) v1.getTablaEventos().getValueAt(v1.getTablaEventos().getSelectedRow(), 2); 
		this.fechaFin=(String) v1.getTablaEventos().getValueAt(v1.getTablaEventos().getSelectedRow(), 3);
		this.idEvento=this.lastSelectedKey;
		
		Date diaInicio=Util.isoStringToDate(fechaInicio);
		Calendar cInicio=Calendar.getInstance();
		cInicio.setTime(diaInicio);
		this.NumDiaInicio=cInicio.get(Calendar.DAY_OF_WEEK);
		Date diaFin=Util.isoStringToDate(fechaFin);
		Calendar cFin=Calendar.getInstance();
		cFin.setTime(diaFin);
		this.NumDiaFin=cFin.get(Calendar.DAY_OF_WEEK);//Domingo=1; Sabado=7
			
		if(NumDiaInicio>=2 && NumDiaFin<=5 && NumDiaInicio<=5 && NumDiaInicio<=NumDiaFin ) {
			this.TipoContrato=2;//El evento es por semana
		}		
		else if(NumDiaInicio>=6 && (NumDiaFin==7 || NumDiaFin==6 || NumDiaFin==1)) {
			this.TipoContrato=1;//El evento es el findesemana
		}
		else {
			this.TipoContrato=3;//El evento coje dias de por semana y del findesemana
		}
		
		if(v1.getComboBoxMostrar().getSelectedItem().equals("residentes")){//Solo mostrar los reporteros residentes en el lugar del evento
			Region=model.getRegionEvento(idEvento);
			List<ReporteroPOJO> eventos=model.getRepDis_TablaRolResidentes(fechaInicio,selectedRole,Region,fechaFin,idEvento,TipoContrato);
			TableModel tmodel=SwingUtil.getTableModelFromPojos(eventos, new String[] {"id_reportero", "nombre"});
			v1.getTablaRepDis().setModel(tmodel);
			SwingUtil.autoAdjustColumns(v1.getTablaRepDis());
		}
		else if(v1.getComboBoxMostrar().getSelectedItem().equals("en la region")){//Mostramos los que esten en la region
			Region=model.getRegionEvento(idEvento);
			Date DiaInicioAnterior=new Date(diaInicio.getTime()-86400000);
			
			String an=Integer.toString(DiaInicioAnterior.getYear()+1900);
			String mes=Integer.toString(DiaInicioAnterior.getMonth()+1);
			String dia=Integer.toString(DiaInicioAnterior.getDate());
			if(dia.length()==1) {
				dia="0"+dia;
			}
			if(mes.length()==1) {
				mes="0"+mes;
			}

			String FechaInicioAnterior = an+"-"+mes+"-"+dia;
			
			List<ReporteroPOJO> eventos=model.getRepDis_TablaRolEnZona(fechaInicio,selectedRole,Region,fechaFin,idEvento,TipoContrato,FechaInicioAnterior);
			TableModel tmodel=SwingUtil.getTableModelFromPojos(eventos, new String[] {"id_reportero", "nombre"});
			v1.getTablaRepDis().setModel(tmodel);
			SwingUtil.autoAdjustColumns(v1.getTablaRepDis());
		}
		else {//Mostramos todos
		List<ReporteroPOJO> eventos=model.getRepDis_TablaRol(fechaInicio,selectedRole,fechaFin,idEvento,TipoContrato);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(eventos, new String[] {"id_reportero", "nombre"});
		v1.getTablaRepDis().setModel(tmodel);
		SwingUtil.autoAdjustColumns(v1.getTablaRepDis());
		}
	}
	
	public void getRoles() {
		List<Object[]> role=model.getRolComboBox();
		ComboBoxModel<Object> lmodel=SwingUtil.getComboModelFromList(role);
		v1.getComboBoxTipo().setModel(lmodel);
	}
	
	
	public void getListaEventos() {
		List<EventoPOJO> eventos=model.getEventos_Tabla();
		TableModel tmodel=SwingUtil.getTableModelFromPojos(eventos, new String[] {"id_evento", "titulo", "diaInicio","diaFin"});
		v1.getTablaEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(v1.getTablaEventos());
	}
	
	
	public void Finalizar() {
		boolean base = false;
		boolean res=false;
		idEvento=this.lastSelectedKey;
		List<ReporteroPOJO> reporteros=model.getRepEnEquipo_TablaRoles(idEvento);
		List<EquipoPOJO> equipo=model.getRepEnEquipo_TablaRes(idEvento);
		
		for (ReporteroPOJO reportero: reporteros) {
			System.out.println(reportero.getRol());
			if (reportero.getRol().equals("base")) {
				base = true;
			}
		}
		
		for (EquipoPOJO equ: equipo) {
			if (equ.getResponsable().equals("1")) {
				res = true;
			}
		}
		
		
		System.out.print(base);
		if (base && res) {
			JOptionPane.showMessageDialog(v1.getFrame(), "Se ha terminado el equipo");
			model.TerminarEquipo(idEvento);
			DefaultTableModel m1= (DefaultTableModel) v1.getTablaRepEvento().getModel();
			DefaultTableModel m= (DefaultTableModel) v1.getTablaRepDis().getModel();
			while(m.getRowCount()>0)m.removeRow(0);
			while(m1.getRowCount()>0)m1.removeRow(0);
			this.getListaEventos();

		}
		else if(!base){
			JOptionPane.showMessageDialog(v1.getFrame(), "Los equipos deben de tener un reportero base","No se encontr� reportero base", JOptionPane.ERROR_MESSAGE);
			}
		else{
			JOptionPane.showMessageDialog(v1.getFrame(), "Los equipos deben de tener un reportero responsable","No se encontro reportero responsable", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	
	
	public void getListaRepEnEquipo() {
	
		this.lastSelectedKey=SwingUtil.getSelectedKey(v1.getTablaEventos());
		idEvento=String.valueOf(this.lastSelectedKey);
		idEvento=this.lastSelectedKey;
		List<ReporteroPOJO> reporteros=model.getRepEnEquipo_Tabla(idEvento);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(reporteros, new String[] {"id_reportero", "nombre"});
		v1.getTablaRepEvento().setModel(tmodel);
		SwingUtil.autoAdjustColumns(v1.getTablaRepEvento());
		
	}
	
	public void Anadir() {	
		this.idReportero=SwingUtil.getSelectedKey(v1.getTablaRepDis());
		model.anadirReportero(idEvento,idReportero);
		this.getReporterosRol();
		this.getListaRepEnEquipo();
	}
		
	public void MarcarRes() {
		this.idResponsable=SwingUtil.getSelectedKey(v1.getTablaRepEvento());
		model.hacerResponsable(idResponsable,idEvento);
		System.out.printf("El reportero %s es el responsable del evento %s \n",idResponsable,idEvento);
		this.getListaRepEnEquipo();
	}
}
