package controlador.reportajes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.TableModel;

import POJOs.InformeEventosReporteroPOJO;
import POJOs.InformeReporterosPOJO;
import modelo.reportajes.modelo;
import util.reportajes.SwingUtil;
import util.reportajes.Util;
import vista.reportajes.InformeEventosGestorView;
import vista.reportajes.InformeEventosReporteroView;

public class InformeEventosGestorControlador {
	
	private modelo modelo;
	private InformeEventosGestorView vista;
	private String id_agencia;
	private List<InformeEventosReporteroPOJO> lista_eventos=new ArrayList<InformeEventosReporteroPOJO>();
	private List<InformeReporterosPOJO> lista_reporteros=new ArrayList<InformeReporterosPOJO>();



	public InformeEventosGestorControlador(modelo m, InformeEventosGestorView v) {
		this.modelo = m;
		this.vista = v;
		this.initView();
	}

	private void initView() {
		vista.getFrame().setVisible(true);

	}

	public void initController() {
		vista.getBtnAceptar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> getListaEventos()));
		vista.getTableEventos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {

				SwingUtil.exceptionWrapper(() -> getReportero());
			}
		});
	}
	
	public void getListaEventos() {
		this.limpiarVentana();
		this.id_agencia=vista.getTextFieldIdAgencia().getText();
		this.lista_eventos=modelo.getEventosAgencia(id_agencia);

		TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_eventos, new String[] {"id_evento","titulo","pais","region","diaInicio","diaFin"});
		vista.getTableEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableEventos());

		
	}

	public void getReportero() {
		
		
		String id_evento= String.valueOf(vista.getTableEventos().getModel().getValueAt(vista.getTableEventos().getSelectedRow(), 0));
		String regionEvento = modelo.getRegionEvento(id_evento);
		String regionReportero;
		if (!"".equals(id_evento)){
			this.lista_reporteros=modelo.getInformeReporteros(id_evento);
			
			//Preparar dietas
			for(InformeReporterosPOJO e: this.lista_reporteros) {
				regionReportero=modelo.getRegionReportero(String.valueOf(e.getId_reportero()));
				System.out.printf("Region Reportero: %s \n", regionReportero);
				if(regionEvento.equals(regionReportero)) {
					e.setDieta_alojamiento(0);
					}
			}
			
			
			TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_reporteros, 
					new String[] {"id_reportero","nombre","dieta_alimentacion","dieta_alojamiento"});
			vista.getTableReporteros().setModel(tmodel);
			SwingUtil.autoAdjustColumns(vista.getTableReporteros());

			//Calcular total dietas
			String diaIni=String.valueOf(vista.getTableEventos().getModel().getValueAt(vista.getTableEventos().getSelectedRow(), 4));
			String diaFin=String.valueOf(vista.getTableEventos().getModel().getValueAt(vista.getTableEventos().getSelectedRow(), 5));

			Date fechaInicio=Util.isoStringToDate(diaIni);
			Date fechaFin=Util.isoStringToDate(diaFin);
			int duracion = (int) ((fechaFin.getTime()-fechaInicio.getTime())/86400000);
			System.out.printf("Duracion: %d\n", duracion);
			
			int total=0;
			for(InformeReporterosPOJO e: this.lista_reporteros) {
				total+= (e.getDieta_alimentacion()+e.getDieta_alojamiento())*duracion;
				System.out.printf("Alimentacion: %d\n", e.getDieta_alimentacion());
				System.out.printf("Alojamiento: %d\n", e.getDieta_alojamiento());
				System.out.printf("Total: %d\n",total);

			}
			vista.getTextFieldDietas().setText(String.valueOf(total) + "�");
		}
	}
	
	public void limpiarVentana() {
		vista.getTextFieldDietas().setText("");
		this.lista_reporteros.removeAll(this.lista_reporteros);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_reporteros, 
				new String[] {"id_reportero","nombre","dieta_alimentacion","dieta_alojamiento"});
		vista.getTableReporteros().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTableReporteros());
	}
}
