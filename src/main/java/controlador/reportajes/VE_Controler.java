package controlador.reportajes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.table.TableModel;

import POJOs.EventoDisplayPOJO;
import POJOs.ReporteroPOJO;
import modelo.reportajes.modelo;
import util.reportajes.SwingUtil;
import vista.reportajes.GREP_vista;
import vista.reportajes.VE_vista;

public class VE_Controler {
	private VE_vista vista;
	private modelo modelo;
	private String evento_seleccionado;
	private boolean reporteros_visibles = false;
	
	public VE_Controler(modelo model, VE_vista vista) {
		this.vista = vista;
		this.modelo = model;
		this.initView();
	}

	private void initView() {
		this.getEventos();
		vista.getFrame().setVisible(true);
		
	}

	private void getEventos() {
		List<EventoDisplayPOJO> eventos=modelo.getEventos_Tabla_Con_Reportero();
		
		TableModel tmodel=SwingUtil.getTableModelFromPojos(eventos, new String[] {"id_evento", "titulo"});
		vista.getTablaEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTablaEventos());
		
	}

	public void getReporteros(String ev) {
		
		List<ReporteroPOJO> reporteros = modelo.getReporteros(ev);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(reporteros, new String[] {"nombre"});
		vista.getTablaReporteros().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTablaReporteros());
		
	}

	public void initController() {
		vista.getTablaEventos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				evento_seleccionado = SwingUtil.getSelectedKey(vista.getTablaEventos());
				getReporteros(evento_seleccionado);
				if (!reporteros_visibles) {
				muestraOpciones();
				}
			
			}

			
		});
		
	}
	

		
		
	
	
			private void muestraOpciones() {
				vista.getLabelReporteros().setVisible(true);
				vista.getTablaReporteros().setVisible(true);
				vista.getScrollPanel().setVisible(true);
			}
			
	
		
	}


