package controlador.reportajes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import javax.swing.ComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;

import POJOs.OfrecimientoPOJO;
import modelo.reportajes.modelo;
import util.reportajes.SwingUtil;
import vista.reportajes.GREP_vista;

public class GREP_Controler {
	private GREP_vista vista;
	private int[] ofrecimientos_seleccionados;
	private boolean opcionesVisibles = false;
	private int id_ofrecimiento = 0;
	private modelo model;
	private int id_empresa;
	private int filtro_seleccionado = 0; //Ning�n filtro de dinero
	private String tematica ="Todas";
	private List<OfrecimientoPOJO> lista_ofrecimientos;
	
	public GREP_Controler(modelo model, GREP_vista vista) {
		this.vista = vista;
		this.model = model;
		this.initView();
		
	}
	
	public void initController() {
		
		vista.getBotonAceptar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> aceptarOfrecimiento()));
		vista.getBotonRechazar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> rechazarOfrecimiento()));
		
		vista.btnAplicar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
				switch (filtro_seleccionado) {
				case 0: 
					getOfrecimientos();
					break;
				case 1:
					getOfrecimientos((int)vista.getMaximoFiltro().getValue());
					break;
				case 2:
					getOfrecimientos((int)vista.getMinimoFiltro().getValue());
					break;
				case 3:
					getOfrecimientos((int)vista.getMaximoFiltro().getValue(),(int)vista.getMinimoFiltro().getValue());
					break;
				default: 
					break;
				}
			}
		});
		
		vista.getComboTematica().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tematica = vista.getComboTematica().getSelectedItem().toString();
				switch (filtro_seleccionado) {
				case 1:
					getOfrecimientos((int)vista.getMaximoFiltro().getValue());
					break;
				case 2:
					getOfrecimientos((int)vista.getMinimoFiltro().getValue());
					break;
				case 3:
					getOfrecimientos((int)vista.getMaximoFiltro().getValue(),(int)vista.getMinimoFiltro().getValue());
					break;
				default: 
					break;
				}
			}
		});
		
		
		
		vista.getFiltroPrecio().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				filtro_seleccionado = vista.getFiltroPrecio().getSelectedIndex();
				System.out.printf("Se ha seleccionado el indice %d de el combobox", filtro_seleccionado);
				AplicaFiltroPrecio();
				
			}
		});
		
		vista.getBotonID().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				id_empresa = Integer.parseInt(vista.getTFid_empresa().getText());
				getOfrecimientos();
				
			}
		});
		vista.getTablaOfrecimientos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				ofrecimientos_seleccionados = vista.getTablaOfrecimientos().getSelectedRows();
				if (!opcionesVisibles) {
				muestraOpciones();
				}
			
			}
			
		});
		
		
	}
	
	public void initView() {
		
		vista.getFrame().setVisible(true);
		getTematicasCB();
	}
	

	public void getOfrecimientos() {
		List<OfrecimientoPOJO> ofrecimientos=model.getOfrecimientos(Integer.toString(id_empresa));
		
		ofrecimientos = trataTematicas(ofrecimientos);
		lista_ofrecimientos = ofrecimientos;
		TableModel tmodel=SwingUtil.getTableModelFromPojos(ofrecimientos, new String[] {"id_evento", "titulo","id_agencia","precio","tematica"});
		vista.getTablaOfrecimientos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTablaOfrecimientos());
	}
	
	public void getOfrecimientos(int limite) {
		List<OfrecimientoPOJO> ofrecimientos = null;
		//Se ha seleccionado maximo
		if (filtro_seleccionado == 1) {
		 ofrecimientos=model.getOfrecimientosMax(Integer.toString(id_empresa),limite);
		}
		//Se ha seleccionado minimo
		if(filtro_seleccionado ==2) {
			ofrecimientos=model.getOfrecimientosMin(Integer.toString(id_empresa),limite);
		}
		ofrecimientos = trataTematicas(ofrecimientos);
		lista_ofrecimientos = ofrecimientos;
		TableModel tmodel=SwingUtil.getTableModelFromPojos(ofrecimientos, new String[] {"id_evento", "titulo","id_agencia","precio","tematica"});
		vista.getTablaOfrecimientos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTablaOfrecimientos());
		filtraTematica();
	}
	
	public void getOfrecimientos(int Maximo, int Minimo) {
		List<OfrecimientoPOJO> ofrecimientos=model.getOfrecimientosMaxMin(Integer.toString(id_empresa),Maximo,Minimo);
	
		ofrecimientos = trataTematicas(ofrecimientos);
		lista_ofrecimientos = ofrecimientos;
		TableModel tmodel=SwingUtil.getTableModelFromPojos(ofrecimientos, new String[] {"id_evento", "titulo","id_agencia","precio","tematica"});
		vista.getTablaOfrecimientos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTablaOfrecimientos());
		filtraTematica();
	}
	
	public List<OfrecimientoPOJO> trataTematicas(List<OfrecimientoPOJO> lista) {
		String current_id;
		String id_evento;
		String tematicas ="";
		List<String> eventosTratados = new ArrayList<>();
		List<OfrecimientoPOJO> aux = new ArrayList<>();
		if (lista.size() != 0) {
		int last_index = 0;
		int aux_last_element = 0;
		
		current_id = lista.get(last_index).getId_evento();
		aux.add(lista.get(last_index));
		
		for (int i = 1 ; i<lista.size();i++) {
			
			id_evento = lista.get(i).getId_evento();
			
			if (current_id.equals(id_evento)) {
				OfrecimientoPOJO p = aux.get(aux_last_element);
				p.setTematica(p.getTematica()+", "+lista.get(i).getTematica());
				aux.set(aux_last_element,p);
			}
			else {
				aux.add(lista.get(i));
				aux_last_element++;
				current_id = id_evento;
			}
		}
		}
		return aux;
	}
	
	public void filtraTematica() {
		for (OfrecimientoPOJO p: lista_ofrecimientos) {
			System.out.println(p.getId_evento());
		}
		if ( !tematica.equals("Todas")) {
			
			lista_ofrecimientos.removeIf(noCumpleTematica(tematica));
			
			TableModel tmodel=SwingUtil.getTableModelFromPojos(lista_ofrecimientos, new String[] {"id_evento", "titulo","id_agencia","precio","tematica"});
			vista.getTablaOfrecimientos().setModel(tmodel);
			SwingUtil.autoAdjustColumns(vista.getTablaOfrecimientos());
		}
		
	}
	
	public void getTematicasCB() {
		List<Object[]> tematica = model.getTematicaComboBox();
		Object[] s = {"Todas"};
		tematica.add(0, s);
		ComboBoxModel<Object> lmodel=SwingUtil.getComboModelFromList(tematica);
		
		vista.getComboTematica().setModel(lmodel);
	}
	
	
	public void aceptarOfrecimiento() {
		for (int ofrecimiento : ofrecimientos_seleccionados) {
		String  id_evento = (String) vista.getTablaOfrecimientos().getValueAt(ofrecimiento, 0);
		String  id_agencia = (String) vista.getTablaOfrecimientos().getValueAt(ofrecimiento, 2);
		model.respondeOfrecimiento(id_evento, id_agencia, "1",id_empresa);
	
		}
		JOptionPane.showMessageDialog(vista.getFrame(), "Se ha aceptado el ofrecimiento");
		getOfrecimientos();
		
	}
	
	public static Predicate<OfrecimientoPOJO> noCumpleTematica(String tematica)
	{
	    return p -> !p.getTematica().contains(tematica);
	}
	
	public void rechazarOfrecimiento() {
		for (int ofrecimiento : ofrecimientos_seleccionados) {
		String  id_evento = (String) vista.getTablaOfrecimientos().getValueAt(ofrecimiento, 0);
		String  id_agencia = (String) vista.getTablaOfrecimientos().getValueAt(ofrecimiento, 2);
		model.respondeOfrecimiento(id_evento, id_agencia, "0",id_empresa);
		}
		JOptionPane.showMessageDialog(vista.getFrame(), "Se ha rechazado el ofrecimiento");
		getOfrecimientos();
	}
	 
	public void AplicaFiltroPrecio() {
		switch (filtro_seleccionado) {
		case 0: 
			vista.getMinimoFiltro().setEnabled(false);
			vista.getMaximoFiltro().setEnabled(false);
			vista.btnAplicar().setEnabled(false);
			vista.getComboTematica().setEnabled(false);
			getOfrecimientos();
			break;
		case 1:
			//Maximo
			vista.getMinimoFiltro().setEnabled(false);
			vista.getMaximoFiltro().setEnabled(true);
			vista.btnAplicar().setEnabled(true);
			vista.getComboTematica().setEnabled(true);
			getOfrecimientos((int)vista.getMaximoFiltro().getValue());
			break;
		case 2:
			vista.getMinimoFiltro().setEnabled(true);
			vista.getMaximoFiltro().setEnabled(false);
			vista.btnAplicar().setEnabled(true);
			vista.getComboTematica().setEnabled(true);
			getOfrecimientos((int)vista.getMinimoFiltro().getValue());
			break;
		case 3:
			vista.getMinimoFiltro().setEnabled(true);
			vista.getMaximoFiltro().setEnabled(true);
			vista.btnAplicar().setEnabled(true);
			vista.getComboTematica().setEnabled(true);
			getOfrecimientos((int)vista.getMaximoFiltro().getValue(),(int)vista.getMinimoFiltro().getValue());
			break;
		default: 
			vista.getMinimoFiltro().setEnabled(false);
			vista.getMaximoFiltro().setEnabled(false);
			vista.btnAplicar().setEnabled(false);
			vista.getComboTematica().setEnabled(false);
			break;
		}
		
		
	}

	
	public void muestraOpciones() {
		vista.getBotonAceptar().setVisible(true);
		vista.getBotonRechazar().setVisible(true);
		vista.getLabelOpciones().setVisible(true);
		opcionesVisibles = true;
		
	}
	
	
	

}
