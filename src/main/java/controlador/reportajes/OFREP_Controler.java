package controlador.reportajes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.TableModel;

import POJOs.EmpresaPOJO;
import POJOs.EventoDisplayPOJO;

import modelo.reportajes.consultas;
import modelo.reportajes.modelo;
import util.reportajes.SwingUtil;
import vista.reportajes.OFREP_vista;

public class OFREP_Controler {
	private OFREP_vista vista;
	private modelo modelo;
	private String evento;
	private int[] id_empresas_seleccionadas;
	private boolean EmpresasVisibles = false;

	private int id_ofrecimiento = 630;

	private int id_agencia;
	private List<EmpresaPOJO> empresas;
	private List<EmpresaPOJO> empresas_ofertadas ;

	public OFREP_Controler (modelo model, OFREP_vista vista){
		this.vista = vista;
		this.modelo = model;
		initView();
	}
	
	
	public void initController() {
		
		vista.getBtnAceptar_IDAgencia().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				id_agencia =Integer.parseInt(vista.getTextFieldID().getText());
				getListaEventos();
				
			}
		});
		
		vista.getBotonAceptar().addActionListener(e -> SwingUtil.exceptionWrapper(() -> crearOfrecimiento()));
		
		vista.getTablaEventos().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				evento = (String) vista.getTablaEventos().getValueAt(vista.getTablaEventos().getSelectedRow(),0);
				if (!EmpresasVisibles) {
				pintaEmpresasOfrecidas();
				muestraEmpresas();
				}
				getListaEmpresas();
				pintaEmpresasOfrecidas();
			}
		});
		
		vista.getTablaEmpresas().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				id_empresas_seleccionadas = SwingUtil.getSelectedKeys(vista.getTablaEmpresas());
			}
		});
	}
	
	
	
	
	
	public void initView() {
		vista.getFrame().setVisible(true);
		
		
	}
	
	public void getListaEventos() {
		List<EventoDisplayPOJO> eventos=modelo.getEventosTabla(id_agencia);
		
		TableModel tmodel=SwingUtil.getTableModelFromPojos(eventos, new String[] {"id_evento", "titulo"});
		vista.getTablaEventos().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTablaEventos());
	}
	
	public void getListaEmpresas() {
		 List<EmpresaPOJO> empresas_modelo=modelo.getEmpresasCom_Tabla(evento);
		 empresas = empresas_modelo;
		 formateaEmpresas(empresas);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(empresas, new String[] {"id_empresa", "nombre","tipo","pagada"});
		vista.getTablaEmpresas().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTablaEmpresas());
	}
	
	private void formateaEmpresas(List<EmpresaPOJO> empresas) {
		for (EmpresaPOJO p: empresas) {
			if (p.getTipo() == null) {
				p.setTipo("----");
			}
			if(p.getPagada() == null) {
				p.setPagada("----");
			}
			if (p.getPagada().equals("0")) {
				p.setPagada("Sin pagar");
			}
			if (p.getPagada().equals("1")) {
				p.setPagada("Pagos al d�a");
			}
			
		}
		
	}


	public void crearOfrecimiento() {
		
		//if (id_empresas_seleccionadas != null) {
		//for (int id : id_empresas_seleccionadas ) {
			//String current = vista.getTablaEmpresas().getValueAt(id, 0).toString();
			int[] empresas_seleccionadas = SwingUtil.getSelectedKeys(vista.getTablaEmpresas());
			//Debug(current,evento);
			if (empresas_seleccionadas != null ) {
			for (int id: id_empresas_seleccionadas) {
			String current = vista.getTablaEmpresas().getValueAt(id, 0).toString();
			System.out.printf("%s ",current);
			modelo.insertaOfrecimientos(id_ofrecimiento,Integer.parseInt(evento), Integer.parseInt(current));
			id_ofrecimiento++;
				}
			}
			
		//}
		//}
		if (vista.getCheckBoxTarifaPlana().isSelected()) {
			
			for (EmpresaPOJO c : empresas) {
				//System.out.printf("empresa %s con tarifa: %s y pagada = %s\n",c.getId_empresa(),c.getTipo(),c.getPagada());
				if (c.getTipo().equals("plana")) {
					if (c.getPagada().equals("Pagos al d�a")) {
						
						modelo.insertaOfrecimientosPLANA(id_ofrecimiento, Integer.parseInt(evento), Integer.parseInt(c.getId_empresa()));
						Debug(c.getId_empresa(),evento);
						id_ofrecimiento++;
					}
				}
			}
			
		}
		
		this.getListaEmpresas();
		pintaEmpresasOfrecidas();
	}
	
	private void pintaEmpresasOfrecidas() {
		 empresas_ofertadas = modelo.getEmpresasCom_TablaIsIn(evento);

		for (EmpresaPOJO c : empresas_ofertadas) {
			System.out.printf("empresa %s con tarifa: %s y pagada = %s\n",c.getId_empresa(),c.getTipo(),c.getPagada());
		}
		 formateaEmpresas(empresas_ofertadas);
		TableModel tmodel=SwingUtil.getTableModelFromPojos(empresas_ofertadas, new String[] {"id_empresa", "nombre","tipo","pagada"});
		vista.getTablaEmpresasTarifa().setModel(tmodel);
		SwingUtil.autoAdjustColumns(vista.getTablaEmpresas());
		
	}


	public void Debug(String id, String evento) {
		System.out.printf("Como la agencia %d he creado el ofrecimiento del evento %s a la empresa %s\n",id_agencia,evento,id);
	}
	
	//Muestra la tabla y la label de empresas cuando seleccionas un evento
	public void muestraEmpresas() {
		vista.getLabelEmpresas().setVisible(true);
		vista.getTablaEmpresas().setVisible(true);
		vista.getScrollPaneEmpresas().setVisible(true);
		vista.getCheckBoxTarifaPlana().setVisible(true);
		vista.getTablaEmpresasTarifa().setVisible(true);
		vista.getScrollPaneEmpresasOfrecidas().setVisible(true);
		vista.getBotonAceptar().setEnabled(true);
		vista.getLabelSeHaOfrecido().setVisible(true);
		EmpresasVisibles = true;
	}
	
	
	
	
}
