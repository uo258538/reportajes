package vista.reportajes;

import java.awt.EventQueue;

import javax.swing.JFrame;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;

public class EmpresaAccederReportajeView {

	private JFrame frame;
	private JTable tableEventos;
	private JTextField textFieldIdEmpresa;
	private JTextField textFieldTitulo;
	private JTextField textFieldSubtitulo;
	private JTextField textFieldCuerpo;
	private JButton btnAceptar;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTable getTableEventos() {
		return tableEventos;
	}

	public void setTableEventos(JTable tableEventos) {
		this.tableEventos = tableEventos;
	}

	public JTextField getTextFieldIdEmpresa() {
		return textFieldIdEmpresa;
	}

	public void setTextFieldIdEmpresa(JTextField textFieldIdEmpresa) {
		this.textFieldIdEmpresa = textFieldIdEmpresa;
	}

	public JTextField getTextFieldTitulo() {
		return textFieldTitulo;
	}

	public void setTextFieldTitulo(JTextField textFieldTitulo) {
		this.textFieldTitulo = textFieldTitulo;
	}

	public JTextField getTextFieldSubtitulo() {
		return textFieldSubtitulo;
	}

	public void setTextFieldSubtitulo(JTextField textFieldSubtitulo) {
		this.textFieldSubtitulo = textFieldSubtitulo;
	}

	public JTextField getTextFieldCuerpo() {
		return textFieldCuerpo;
	}

	public void setTextFieldCuerpo(JTextField textFieldCuerpo) {
		this.textFieldCuerpo = textFieldCuerpo;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	/**
	 * Create the application.
	 */
	public EmpresaAccederReportajeView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 904, 471);
		frame.setTitle("Acceder a un Reportaje");
		frame.setName("Acceder a un Reportaje");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Lista de Eventos");
		label.setBounds(20, 151, 102, 14);
		frame.getContentPane().add(label);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 176, 368, 232);
		frame.getContentPane().add(scrollPane);
		
		tableEventos = new JTable();
		scrollPane.setViewportView(tableEventos);
		
		JLabel label_1 = new JLabel("ID Empresa Comunicaci\u00F3n");
		label_1.setBounds(29, 71, 123, 14);
		frame.getContentPane().add(label_1);
		
		textFieldIdEmpresa = new JTextField();
		textFieldIdEmpresa.setColumns(10);
		textFieldIdEmpresa.setBounds(187, 68, 86, 20);
		frame.getContentPane().add(textFieldIdEmpresa);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(187, 99, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		JLabel label_2 = new JLabel("Contenido del Reportaje Seleccionado");
		label_2.setBounds(462, 71, 221, 14);
		frame.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("Titulo");
		label_3.setBounds(476, 129, 46, 14);
		frame.getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("Subt\u00EDtulo");
		label_4.setBounds(476, 193, 78, 14);
		frame.getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("Cuerpo");
		label_5.setBounds(476, 252, 78, 14);
		frame.getContentPane().add(label_5);
		
		textFieldTitulo = new JTextField();
		textFieldTitulo.setEditable(false);
		textFieldTitulo.setColumns(10);
		textFieldTitulo.setBounds(476, 161, 265, 20);
		frame.getContentPane().add(textFieldTitulo);
		
		textFieldSubtitulo = new JTextField();
		textFieldSubtitulo.setEditable(false);
		textFieldSubtitulo.setColumns(10);
		textFieldSubtitulo.setBounds(476, 221, 265, 20);
		frame.getContentPane().add(textFieldSubtitulo);
		
		textFieldCuerpo = new JTextField();
		textFieldCuerpo.setEditable(false);
		textFieldCuerpo.setColumns(10);
		textFieldCuerpo.setBounds(476, 286, 265, 110);
		frame.getContentPane().add(textFieldCuerpo);
	}
}
