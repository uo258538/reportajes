package vista.reportajes;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Menu_vista {

	private JFrame frame;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JButton btnAsigRep;
	private JButton btnDarAcceso;
	private JButton ImpDieta;
	private JButton VerOfre;
	private JButton btnInforme;
	private JButton btnInformeDeReportajes;
	private JButton btnDsitribuirReportaje;
	private JButton GestRep;
	private JButton btnNVerRep;
	private JButton btnVerReporterosDe;
	private JButton btnInformeDeUn;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu_vista window = new Menu_vista();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Menu_vista() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 757, 445);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblNewLabel = new JLabel("Gestor");
		lblNewLabel.setBounds(73, 16, 69, 20);
		frame.getContentPane().add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Empresa de Comunicacion");
		lblNewLabel_1.setBounds(262, 16, 203, 20);
		frame.getContentPane().add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("Reportero");
		lblNewLabel_2.setBounds(568, 16, 139, 20);
		frame.getContentPane().add(lblNewLabel_2);
		
		btnAsigRep = new JButton("Asignar reporteros a eventos");
		btnAsigRep.setBounds(0, 52, 210, 29);
		frame.getContentPane().add(btnAsigRep);
		
		btnDarAcceso = new JButton("Dar acceso a un reportaje");
		btnDarAcceso.setBounds(0, 97, 210, 29);
		frame.getContentPane().add(btnDarAcceso);
		
		ImpDieta = new JButton("Ver importe dieta");
		ImpDieta.setBounds(504, 52, 203, 29);
		frame.getContentPane().add(ImpDieta);
		
		VerOfre = new JButton("Ver ofrecimientos");
		VerOfre.setBounds(248, 52, 203, 29);
		frame.getContentPane().add(VerOfre);
		
		btnInforme = new JButton("Informe de eventos cubiertos");
		btnInforme.setBounds(504, 97, 203, 29);
		frame.getContentPane().add(btnInforme);
		
		btnInformeDeReportajes = new JButton("Informe de reportajes");
		btnInformeDeReportajes.setBounds(246, 97, 205, 29);
		frame.getContentPane().add(btnInformeDeReportajes);
		
		btnDsitribuirReportaje = new JButton("Ofrecer reportaje");
		btnDsitribuirReportaje.setBounds(0, 147, 210, 29);
		frame.getContentPane().add(btnDsitribuirReportaje);
		
		GestRep = new JButton("Gestion reportaje");
		GestRep.setBounds(504, 147, 203, 29);
		frame.getContentPane().add(GestRep);
		
		btnNVerRep = new JButton("Ver reportaje");
		btnNVerRep.setBounds(246, 142, 205, 29);
		frame.getContentPane().add(btnNVerRep);
		
		btnVerReporterosDe = new JButton("Ver reporteros evento");
		btnVerReporterosDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnVerReporterosDe.setBounds(246, 192, 205, 29);
		frame.getContentPane().add(btnVerReporterosDe);
		
		btnInformeDeUn = new JButton("Informe de un evento");
		btnInformeDeUn.setBounds(0, 192, 210, 29);
		frame.getContentPane().add(btnInformeDeUn);
		
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JLabel getLblNewLabel() {
		return lblNewLabel;
	}

	public void setLblNewLabel(JLabel lblNewLabel) {
		this.lblNewLabel = lblNewLabel;
	}

	public JLabel getLblNewLabel_1() {
		return lblNewLabel_1;
	}

	public void setLblNewLabel_1(JLabel lblNewLabel_1) {
		this.lblNewLabel_1 = lblNewLabel_1;
	}

	public JLabel getLblNewLabel_2() {
		return lblNewLabel_2;
	}

	public void setLblNewLabel_2(JLabel lblNewLabel_2) {
		this.lblNewLabel_2 = lblNewLabel_2;
	}

	public JButton getBtnAsigRep() {
		return btnAsigRep;
	}

	public void setBtnAsigRep(JButton btnAsigRep) {
		this.btnAsigRep = btnAsigRep;
	}

	public JButton getBtnDarAcceso() {
		return btnDarAcceso;
	}

	public void setBtnDarAcceso(JButton btnDarAcceso) {
		this.btnDarAcceso = btnDarAcceso;
	}

	public JButton getImpDieta() {
		return ImpDieta;
	}

	public void setImpDieta(JButton impDieta) {
		ImpDieta = impDieta;
	}

	public JButton getVerOfre() {
		return VerOfre;
	}

	public void setVerOfre(JButton verOfre) {
		VerOfre = verOfre;
	}

	public JButton getBtnInforme() {
		return btnInforme;
	}

	public void setBtnInforme(JButton btnInforme) {
		this.btnInforme = btnInforme;
	}

	public JButton getBtnInformeDeReportajes() {
		return btnInformeDeReportajes;
	}

	public void setBtnInformeDeReportajes(JButton btnInformeDeReportajes) {
		this.btnInformeDeReportajes = btnInformeDeReportajes;
	}

	public JButton getBtnDsitribuirReportaje() {
		return btnDsitribuirReportaje;
	}

	public void setBtnDsitribuirReportaje(JButton btnDsitribuirReportaje) {
		this.btnDsitribuirReportaje = btnDsitribuirReportaje;
	}

	public JButton getGestRep() {
		return GestRep;
	}

	public void setGestRep(JButton gestRep) {
		GestRep = gestRep;
	}

	public JButton getBtnNVerRep() {
		return btnNVerRep;
	}

	public void setBtnNVerRep(JButton btnNVerRep) {
		this.btnNVerRep = btnNVerRep;
	}

	public JButton getBtnVerReporterosDe() {
		return btnVerReporterosDe;
	}

	public void setBtnVerReporterosDe(JButton btnVerReporterosDe) {
		this.btnVerReporterosDe = btnVerReporterosDe;
	}

	public JButton getBtnInformeDeUn() {
		return btnInformeDeUn;
	}

	public void setBtnInformeDeUn(JButton btnInformeDeUn) {
		this.btnInformeDeUn = btnInformeDeUn;
	}
}
