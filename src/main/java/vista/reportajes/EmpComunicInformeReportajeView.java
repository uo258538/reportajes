package vista.reportajes;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import lombok.Getter;
import lombok.Setter;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;


@Getter
@Setter
public class EmpComunicInformeReportajeView {

	private JFrame frame;
	private JTextField textFieldEmpComunic;
	private JButton btnAceptar;
	private JLabel lblInformeDeEventos;
	private JTable tableReportajes;
	private JScrollPane scrollPane;
	private JLabel lblPrecioDeTodos;
	private JTextField textFieldPrecioTotal;
	private JLabel lblFechaInicio;
	private JTextField textFieldFiltroFechaIni;
	private JTextField textFieldFiltroFechaF;
	private JLabel lblFechaFin;
	private JButton btnAplicarFiltroFecha;

	

	
	/**
	 * Create the application.
	 */
	public EmpComunicInformeReportajeView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 761, 653);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Informe Reportaje Empresa de Comunicacion");
		frame.setName("Informe Reportaje Empresa de Comunicacion");
		
		JLabel lblIdEmpresaComunicacin = new JLabel("Id Empresa Comunicaci\u00F3n");
		lblIdEmpresaComunicacin.setBounds(10, 11, 122, 14);
		frame.getContentPane().add(lblIdEmpresaComunicacin);
		
		textFieldEmpComunic = new JTextField();
		textFieldEmpComunic.setBounds(20, 31, 109, 20);
		frame.getContentPane().add(textFieldEmpComunic);
		textFieldEmpComunic.setColumns(10);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(30, 62, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		lblInformeDeEventos = new JLabel("Lista de Reportajes");
		lblInformeDeEventos.setBounds(10, 213, 122, 14);
		frame.getContentPane().add(lblInformeDeEventos);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 238, 698, 255);
		frame.getContentPane().add(scrollPane);
		
		tableReportajes = new JTable();
		scrollPane.setViewportView(tableReportajes);
		tableReportajes.setDefaultEditor(Object.class, null); //readonly

		
		lblPrecioDeTodos = new JLabel("Precio de todos los eventos en euros");
		lblPrecioDeTodos.setBounds(10, 516, 229, 14);
		frame.getContentPane().add(lblPrecioDeTodos);
		
		textFieldPrecioTotal = new JTextField();
		textFieldPrecioTotal.setEditable(false);
		textFieldPrecioTotal.setBounds(20, 541, 150, 20);
		frame.getContentPane().add(textFieldPrecioTotal);
		textFieldPrecioTotal.setColumns(10);
		
		lblFechaInicio = new JLabel("Fecha Inicio");
		lblFechaInicio.setBounds(10, 96, 89, 14);
		frame.getContentPane().add(lblFechaInicio);
		
		textFieldFiltroFechaIni = new JTextField();
		textFieldFiltroFechaIni.setBounds(20, 121, 86, 20);
		frame.getContentPane().add(textFieldFiltroFechaIni);
		textFieldFiltroFechaIni.setColumns(10);
		
		textFieldFiltroFechaF = new JTextField();
		textFieldFiltroFechaF.setBounds(116, 121, 86, 20);
		frame.getContentPane().add(textFieldFiltroFechaF);
		textFieldFiltroFechaF.setColumns(10);
		
		lblFechaFin = new JLabel("Fecha Fin");
		lblFechaFin.setBounds(109, 96, 86, 14);
		frame.getContentPane().add(lblFechaFin);
		
		btnAplicarFiltroFecha = new JButton("Aplicar Filtro Fecha");
		btnAplicarFiltroFecha.setEnabled(false);
		btnAplicarFiltroFecha.setBounds(30, 161, 172, 23);
		frame.getContentPane().add(btnAplicarFiltroFecha);
	}
}
