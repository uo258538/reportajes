package vista.reportajes;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class ReporteroImporteDietaView {

	private JFrame frame;
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getTextFieldIdReportero() {
		return textFieldIdReportero;
	}

	public void setTextFieldIdReportero(JTextField textFieldIdReportero) {
		this.textFieldIdReportero = textFieldIdReportero;
	}

	public JTable getTableEventos() {
		return tableEventos;
	}

	public void setTableEventos(JTable tableEventos) {
		this.tableEventos = tableEventos;
	}

	public JTextField getTextFieldImporte() {
		return textFieldImporte;
	}

	public void setTextFieldImporte(JTextField textFieldImporte) {
		this.textFieldImporte = textFieldImporte;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	private JTextField textFieldIdReportero;
	private JTable tableEventos;
	private JTextField textFieldImporte;
	private JButton btnAceptar;
	private JLabel lblDietasEnEuros;


	/**
	 * Create the application.
	 */
	public ReporteroImporteDietaView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 552, 350);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Visualizar Importe Dietas");
		frame.setName("Visualizar Importe Dietas");
		
		JLabel lblIdreportero = new JLabel("Id_Reportero");
		lblIdreportero.setBounds(26, 11, 93, 14);
		frame.getContentPane().add(lblIdreportero);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(26, 67, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		textFieldIdReportero = new JTextField();
		textFieldIdReportero.setBounds(26, 36, 86, 20);
		frame.getContentPane().add(textFieldIdReportero);
		textFieldIdReportero.setColumns(10);
		
		JLabel lblListaDeEventos = new JLabel("Lista de Eventos");
		lblListaDeEventos.setBounds(26, 124, 93, 14);
		frame.getContentPane().add(lblListaDeEventos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 149, 264, 129);
		frame.getContentPane().add(scrollPane);
		
		tableEventos = new JTable();
		scrollPane.setViewportView(tableEventos);
		tableEventos.setDefaultEditor(Object.class, null); //readonly

		
		textFieldImporte = new JTextField();
		textFieldImporte.setEditable(false);
		textFieldImporte.setBounds(324, 200, 166, 23);
		frame.getContentPane().add(textFieldImporte);
		textFieldImporte.setColumns(10);
		
		JLabel lblImporteTotalA = new JLabel("Importe total a cobrar por\r\n");
		lblImporteTotalA.setBounds(324, 153, 189, 23);
		frame.getContentPane().add(lblImporteTotalA);
		
		lblDietasEnEuros = new JLabel("dietas en euros");
		lblDietasEnEuros.setBounds(324, 175, 143, 14);
		frame.getContentPane().add(lblDietasEnEuros);
	}
}
