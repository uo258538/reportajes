package vista.reportajes;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import lombok.Getter;
import lombok.Setter;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;


public class InformeEventosGestorView {

	private JFrame frame;
	private JTextField textFieldIdAgencia;
	private JButton btnAceptar;
	private JLabel lblListaDeEventos;
	private JTable tableEventos;
	private JScrollPane scrollPane;
	private JLabel lblListaDeReporteros;
	private JTable tableReporteros;
	private JScrollPane scrollPane_1;
	private JLabel lblImportePorDietas;
	private JTextField textFieldDietas;

	/**
	 * Create the application.
	 */
	public InformeEventosGestorView() {
		initialize();
	}
	
	

	public JFrame getFrame() {
		return frame;
	}



	public void setFrame(JFrame frame) {
		this.frame = frame;
	}



	public JTextField getTextFieldIdAgencia() {
		return textFieldIdAgencia;
	}



	public void setTextFieldIdAgencia(JTextField textFieldIdAgencia) {
		this.textFieldIdAgencia = textFieldIdAgencia;
	}



	public JButton getBtnAceptar() {
		return btnAceptar;
	}



	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}



	public JLabel getLblListaDeEventos() {
		return lblListaDeEventos;
	}



	public void setLblListaDeEventos(JLabel lblListaDeEventos) {
		this.lblListaDeEventos = lblListaDeEventos;
	}



	public JTable getTableEventos() {
		return tableEventos;
	}



	public void setTableEventos(JTable tableEventos) {
		this.tableEventos = tableEventos;
	}



	public JScrollPane getScrollPane() {
		return scrollPane;
	}



	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}



	public JLabel getLblListaDeReporteros() {
		return lblListaDeReporteros;
	}



	public void setLblListaDeReporteros(JLabel lblListaDeReporteros) {
		this.lblListaDeReporteros = lblListaDeReporteros;
	}



	public JTable getTableReporteros() {
		return tableReporteros;
	}



	public void setTableReporteros(JTable tableReporteros) {
		this.tableReporteros = tableReporteros;
	}



	public JScrollPane getScrollPane_1() {
		return scrollPane_1;
	}



	public void setScrollPane_1(JScrollPane scrollPane_1) {
		this.scrollPane_1 = scrollPane_1;
	}



	public JLabel getLblImportePorDietas() {
		return lblImportePorDietas;
	}



	public void setLblImportePorDietas(JLabel lblImportePorDietas) {
		this.lblImportePorDietas = lblImportePorDietas;
	}



	public JTextField getTextFieldDietas() {
		return textFieldDietas;
	}



	public void setTextFieldDietas(JTextField textFieldDietas) {
		this.textFieldDietas = textFieldDietas;
	}



	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 721, 687);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Informe Eventos Agencia Prensa");

		JLabel lblIdAgenciaPrensa = new JLabel("Id Agencia Prensa");
		lblIdAgenciaPrensa.setBounds(10, 11, 145, 14);
		frame.getContentPane().add(lblIdAgenciaPrensa);

		textFieldIdAgencia = new JTextField();
		textFieldIdAgencia.setBounds(10, 35, 99, 20);
		frame.getContentPane().add(textFieldIdAgencia);
		textFieldIdAgencia.setColumns(10);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(20, 66, 89, 23);
		frame.getContentPane().add(btnAceptar);

		lblListaDeEventos = new JLabel("Lista de Eventos");
		lblListaDeEventos.setBounds(10, 108, 162, 14);
		frame.getContentPane().add(lblListaDeEventos);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 130, 582, 174);
		frame.getContentPane().add(scrollPane);

		tableEventos = new JTable();
		scrollPane.setViewportView(tableEventos);

		lblListaDeReporteros = new JLabel("Lista de Reporteros");
		lblListaDeReporteros.setBounds(10, 334, 145, 14);
		frame.getContentPane().add(lblListaDeReporteros);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(20, 359, 580, 174);
		frame.getContentPane().add(scrollPane_1);

		tableReporteros = new JTable();
		scrollPane_1.setViewportView(tableReporteros);

		lblImportePorDietas = new JLabel("Importe por dietas de todos los reporteros");
		lblImportePorDietas.setBounds(10, 548, 224, 14);
		frame.getContentPane().add(lblImportePorDietas);

		textFieldDietas = new JTextField();
		textFieldDietas.setEditable(false);
		textFieldDietas.setBounds(20, 575, 135, 20);
		frame.getContentPane().add(textFieldDietas);
		textFieldDietas.setColumns(10);
	}
}
