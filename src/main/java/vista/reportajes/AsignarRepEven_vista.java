package vista.reportajes;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.Dimension;
import java.awt.SystemColor;
import javax.swing.UIManager;
import net.miginfocom.swing.MigLayout;
import javax.swing.SwingConstants;
import java.awt.ScrollPane;
import javax.swing.JTextArea;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;

public class AsignarRepEven_vista {
	private JFrame frame;
	private JPanel contentPane;
	private JPanel panel;
	private JLabel lblListaDeEventos;
	private JTable TablaEventos;
	private JScrollPane scrollPaneEventos;
	private JSeparator separator;
	private JLabel lblRepDis;
	private JTable TablaRepDis;
	private JScrollPane scrollPaneRepDis;
	private JButton Anadir;
	private JLabel lblRepEvento;
	private JTable TableRepEvento;
	private JScrollPane scrollPaneRepEven;
	private JButton btnMarcarComoResponsable;
	private JComboBox<Object> comboBoxTipo;
	private JButton btnNewButton;
	private JLabel lblMostrar;
	private JComboBox<Object> comboBoxMostrar;
	
	
	/**
	 * Launch the application.
	 */
	public AsignarRepEven_vista() {
		initialize();
	}

	/**
	 * Create the frame.
	 */
	public void initialize() {
		
		frame = new JFrame();
		frame.setTitle("Asignar Reporteros a eventos");
		frame.setName("AsignarRepEven_vista");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		
		lblListaDeEventos = new JLabel("Lista de eventos sin reportero asignado");
		lblListaDeEventos.setBounds(136, 0, 293, 53);
		panel.add(lblListaDeEventos);
				
	
		
		scrollPaneEventos = new JScrollPane();
		scrollPaneEventos.setBounds(140, 50, 276, 150);
		panel.add(scrollPaneEventos);
		TablaEventos = new JTable();
		scrollPaneEventos.setViewportView(TablaEventos);
		TablaEventos.setName("TablaEventos");
		TablaEventos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
/////////////////////////////////////////////////////////		
		separator = new JSeparator();
		separator.setBounds(27, 221, 493, 16);
		panel.add(separator);
				
		lblRepDis = new JLabel("Reporteros Disponibles");
		lblRepDis.setBounds(27, 221, 193, 53);
		panel.add(lblRepDis);
		TablaEventos.setDefaultEditor(Object.class, null); //readonly
		
		scrollPaneRepDis = new JScrollPane();
		scrollPaneRepDis.setBounds(37, 265, 129, 150);
		panel.add(scrollPaneRepDis);
		
		TablaRepDis = new JTable();
		scrollPaneRepDis.setViewportView(TablaRepDis);
		
		Anadir = new JButton("Anadir");
		Anadir.setBounds(207, 312, 115, 29);
		panel.add(Anadir);
		
		lblRepEvento = new JLabel("Reporteros asignados");
		lblRepEvento.setBounds(327, 221, 193, 53);
		panel.add(lblRepEvento);
		
		scrollPaneRepEven = new JScrollPane();
		scrollPaneRepEven.setBounds(337, 265, 129, 150);
		panel.add(scrollPaneRepEven);
		
		TableRepEvento = new JTable();
		scrollPaneRepEven.setViewportView(TableRepEvento);
		
		btnMarcarComoResponsable = new JButton("Marcar como responsable");
		btnMarcarComoResponsable.setBounds(302, 426, 228, 29);
		panel.add(btnMarcarComoResponsable);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setBounds(15, 433, 46, 14);
		panel.add(lblTipo);
		
		comboBoxTipo = new JComboBox();
		comboBoxTipo.setEnabled(false);
		comboBoxTipo.setBounds(81, 431, 101, 20);
		panel.add(comboBoxTipo);
		
		btnNewButton = new JButton("FINALIZAR");
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(302, 466, 228, 23);
		panel.add(btnNewButton);
		
		lblMostrar = new JLabel("Mostrar:");
		lblMostrar.setBounds(15, 463, 69, 20);
		panel.add(lblMostrar);
		
		comboBoxMostrar = new JComboBox();
		comboBoxMostrar.setModel(new DefaultComboBoxModel(new String[] {"todos", "residentes", "en la region"}));
		comboBoxMostrar.setEnabled(false);
		comboBoxMostrar.setBounds(81, 466, 101, 20);
		panel.add(comboBoxMostrar);
		
	
		
//////////////////////////////////////////////////////////////////////		
		}
	public JFrame getFrame() { return this.frame;};
	public JTable getTablaEventos() { return this.TablaEventos;}
	public JTable getTablaRepDis() { return this.TablaRepDis;}
	public JTable getTablaRepEvento() { return this.TableRepEvento;}
	public JButton getBotonAnadir() {return this.Anadir;}
	public JButton getBotonMarcarResponsable() {return this.btnMarcarComoResponsable;}
	public JScrollPane getScrollTablaEventos() {return this.scrollPaneEventos;}
	public JScrollPane getScrollRepDis() {return this.scrollPaneRepDis;}
	public JScrollPane getScrollTableRepEvento() {return this.scrollPaneRepEven;}
	public JComboBox<Object> getComboBoxTipo() {return this.comboBoxTipo;}
	public JButton getBotonFinalizar() { return this.btnNewButton;}
	public JComboBox<Object> getComboBoxMostrar() {return this.comboBoxMostrar;}
}
