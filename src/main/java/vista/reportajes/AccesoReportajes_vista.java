package vista.reportajes;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.Dimension;
import java.awt.SystemColor;
import javax.swing.UIManager;
import net.miginfocom.swing.MigLayout;
import javax.swing.SwingConstants;
import java.awt.ScrollPane;
import javax.swing.JTextArea;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AccesoReportajes_vista {
	private JFrame frame;
	private JPanel panel;
	private JLabel lblListaDeEventos;
	private JTable TablaEventos;
	private JScrollPane scrollPaneEventos;
	private JLabel lblEmpresasOfrecimiento;
	private JTable EmpresasOfrecimiento;
	private JButton PermitirAcceso;
	private JScrollPane scrollPaneEmprOfre;
	private JLabel lblSinAccesoY;
	
	public AccesoReportajes_vista() {
		initialize();
	}

	public void initialize() {
		
		frame = new JFrame();
		frame.setTitle("Permitir acceso a los reportajes");
		frame.setName("AccesoReportajes_vista");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		
		lblListaDeEventos = new JLabel("Eventos con reportaje terminado");
		lblListaDeEventos.setBounds(15, 0, 258, 53);
		panel.add(lblListaDeEventos);
				
	
		
		scrollPaneEventos = new JScrollPane();
		scrollPaneEventos.setBounds(15, 69, 175, 150);
		panel.add(scrollPaneEventos);
		TablaEventos = new JTable();
		scrollPaneEventos.setViewportView(TablaEventos);
		TablaEventos.setName("TablaEventos");
		TablaEventos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				
		lblEmpresasOfrecimiento = new JLabel("Empresas con ofrecimiento aceptado, ");
		lblEmpresasOfrecimiento.setBounds(276, 0, 322, 53);
		panel.add(lblEmpresasOfrecimiento);
		TablaEventos.setDefaultEditor(Object.class, null); 
		
		scrollPaneEmprOfre = new JScrollPane();
		scrollPaneEmprOfre.setBounds(276, 69, 233, 150);
		panel.add(scrollPaneEmprOfre);
		
		EmpresasOfrecimiento = new JTable();
		scrollPaneEmprOfre.setViewportView(EmpresasOfrecimiento);
		
		PermitirAcceso = new JButton("Permitir Acceso");
		PermitirAcceso.setBounds(524, 122, 165, 29);
		panel.add(PermitirAcceso);
		
		lblSinAccesoY = new JLabel("sin acceso y pagado");
		lblSinAccesoY.setBounds(317, 41, 141, 20);
		panel.add(lblSinAccesoY);
		
			
		
//////////////////////////////////////////////////////////////////////		
		}
	public JFrame getFrame() { return this.frame;};
	public JTable getTablaEventos() { return this.TablaEventos;}
	public JTable getTablaEmpresasOfrecimiento() { return this.EmpresasOfrecimiento;}
	public JButton getBotonPermitirAcceso() {return this.PermitirAcceso;}
	public JScrollPane getScrollTablaEventos() {return this.scrollPaneEventos;}
	public JScrollPane getScrollRepDis() {return this.scrollPaneEmprOfre;}
}
