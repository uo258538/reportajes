package vista.reportajes;

import java.awt.EventQueue;
import java.awt.TextField;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

public class OFREP_vista {

	private JFrame frmOfrecerReportaje;
	private JButton btnAceptar;
	private JLabel lblEmpresasDeComunicacin;
	private JTable tabla_eventos;
	private JTable tabla_empresas;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	private JButton btnAceptar_1;
	private JTextField TFidagencia;
	
	

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the application.
	 */
	public OFREP_vista() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmOfrecerReportaje = new JFrame();
		frmOfrecerReportaje.setTitle("Ofrecer Reportaje");


		frmOfrecerReportaje.setBounds(100, 100, 680, 523);
		frmOfrecerReportaje.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		frmOfrecerReportaje.getContentPane().setLayout(null);
		
		JLabel lblEventosConReportero = new JLabel("Eventos con reportero:");
		lblEventosConReportero.setBounds(36, 72, 135, 14);
		frmOfrecerReportaje.getContentPane().add(lblEventosConReportero);
		
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setEnabled(false);
		
		btnAceptar.setBounds(529, 423, 89, 36);
		frmOfrecerReportaje.getContentPane().add(btnAceptar);
		
		lblEmpresasDeComunicacin = new JLabel("Empresas de comunicaci\u00F3n:");
		lblEmpresasDeComunicacin.setVisible(false);
		lblEmpresasDeComunicacin.setBounds(307, 72, 226, 14);
		frmOfrecerReportaje.getContentPane().add(lblEmpresasDeComunicacin);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(36, 97, 202, 139);
		frmOfrecerReportaje.getContentPane().add(scrollPane);
		
		tabla_eventos = new JTable();
		tabla_eventos.setModel(tableModel);
		scrollPane.setViewportView(tabla_eventos);
		tabla_eventos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tabla_eventos.setDefaultEditor(Object.class, null); 
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setVisible(false);
		scrollPane_1.setBounds(305, 97, 313, 139);
		frmOfrecerReportaje.getContentPane().add(scrollPane_1);
		
		tabla_empresas = new JTable();
		scrollPane_1.setViewportView(tabla_empresas);
		tabla_empresas.setModel(tableModel);
		
		JLabel lblAgencia = new JLabel("Agencia:");
		lblAgencia.setBounds(45, 28, 70, 14);
		frmOfrecerReportaje.getContentPane().add(lblAgencia);
		
		TFidagencia = new JTextField();
		TFidagencia.setBounds(101, 25, 86, 20);
		frmOfrecerReportaje.getContentPane().add(TFidagencia);
		TFidagencia.setColumns(10);
		
		btnAceptar_1 = new JButton("Aceptar");
		btnAceptar_1.setBounds(212, 24, 89, 23);
		frmOfrecerReportaje.getContentPane().add(btnAceptar_1);
		
		chckbxTarifaPlana = new JCheckBox("Tarifa Plana");
		chckbxTarifaPlana.setVisible(false);
		chckbxTarifaPlana.setBounds(307, 243, 97, 23);
		frmOfrecerReportaje.getContentPane().add(chckbxTarifaPlana);
		
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(36, 281, 452, 178);
		frmOfrecerReportaje.getContentPane().add(scrollPane_2);
		
		table = new JTable();
		table.setModel(tableModel);
		scrollPane_2.setViewportView(table);
		table.setVisible(false);
		scrollPane_2.setVisible(false);
		table.setRowSelectionAllowed(false);
		
		
		lblSeHaOfrecido = new JLabel("Se ha ofrecido a las empresas:");
		lblSeHaOfrecido.setBounds(36, 252, 202, 14);
		lblSeHaOfrecido.setVisible(false);
		frmOfrecerReportaje.getContentPane().add(lblSeHaOfrecido);
		tabla_empresas.setDefaultEditor(Object.class, null); 
		tabla_empresas.setVisible(false);
	}
	public JFrame getFrame() { return this.frmOfrecerReportaje;};
	public JTable getTablaEventos() { return this.tabla_eventos;}
	public JTable getTablaEmpresas() { return this.tabla_empresas;}
	public JButton getBotonAceptar() {return this.btnAceptar;}
	public JLabel getLabelEmpresas() { return this.lblEmpresasDeComunicacin;}
	public JScrollPane getScrollPaneEmpresas() {return this.scrollPane_1;}
	public JScrollPane getScrollPaneEmpresasOfrecidas() {return this.scrollPane_2;}
	public JButton getBtnAceptar_IDAgencia() {return this.btnAceptar_1;}
	public JTextField getTextFieldID() { return this.TFidagencia;} 
	public JTable getTablaEmpresasTarifa() { return this.table;}
	public JCheckBox getCheckBoxTarifaPlana() { return this.chckbxTarifaPlana;}
	public JLabel getLabelSeHaOfrecido() {return this.lblSeHaOfrecido;}
	
	DefaultTableModel tableModel = new DefaultTableModel() {
	    @Override
	    public boolean isCellEditable(int row, int column) {
	       //all cells false
	       return false;
	    }
	};
	private JTable table;
	private JScrollPane scrollPane_2;
	private JCheckBox chckbxTarifaPlana;
	private JLabel lblSeHaOfrecido;
}
