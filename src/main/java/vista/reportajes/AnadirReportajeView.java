package vista.reportajes;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import lombok.Getter;
import lombok.Setter;

import java.awt.Color;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
public class AnadirReportajeView {

	private JFrame frame;
	private JTextField textFieldTitulo;
	private JTextField textFieldSubtitulo;
	private JButton btnAnadirReportaje;
	private JTextPane textPaneCuerpo;
	private JTable tableEventos;
	private JScrollPane scrollPane;
	private JTextField textFieldIdReportero;
	private JButton btnAceptar;
	private JLabel lblMultimedia;
	private JTable tableMultimedia;
	private JButton btnEliminar;
	private JPanel panel_1;
	private JTextField textFieldRuta;
	private JButton btnAnadirMultimedia;
	private JScrollPane scrollPane_1;
	private JCheckBox chckbxRevisado;
	private JTextPane textPaneComentario;
	private JButton btnGuardarComentario;
	private JTable tableEventosConReportajes;
	private JPanel panelRevision;
	private JTable tableComentarios;
	private JButton btnActualizarReportaje;
	private JButton btnNewButton;


	public JButton getBtnNewButton() {
		return btnNewButton;
	}

	public void setBtnNewButton(JButton btnNewButton) {
		this.btnNewButton = btnNewButton;
	}

	public JPanel getPanelRevision() {
		return panelRevision;
	}

	public void setPanelRevision(JPanel panelRevision) {
		this.panelRevision = panelRevision;
	}

	public JTextPane getTextPaneCuerpo() {
		return textPaneCuerpo;
	}

	public void setTextPaneCuerpo(JTextPane textPaneCuerpo) {
		this.textPaneCuerpo = textPaneCuerpo;
	}

	/**
	 * Launch the application.
	 */
	

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getTextFieldTitulo() {
		return textFieldTitulo;
	}

	public void setTextFieldTitulo(JTextField textFieldTitulo) {
		this.textFieldTitulo = textFieldTitulo;
	}

	public JTextField getTextFieldSubtitulo() {
		return textFieldSubtitulo;
	}

	public void setTextFieldSubtitulo(JTextField textFieldSubtitulo) {
		this.textFieldSubtitulo = textFieldSubtitulo;
	}

	public JButton getBtnAnadirReportaje() {
		return btnAnadirReportaje;
	}

	public void setBtnAnadirReportaje(JButton btnAnadirReportaje) {
		this.btnAnadirReportaje = btnAnadirReportaje;
	}

	public JTable getTableEventos() {
		return tableEventos;
	}

	public void setTableEventos(JTable tableEventos) {
		this.tableEventos = tableEventos;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JTextField getTextFieldIdReportero() {
		return textFieldIdReportero;
	}

	public void setTextFieldIdReportero(JTextField textFieldIdReportero) {
		this.textFieldIdReportero = textFieldIdReportero;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	/**
	 * Create the application.
	 */
	public AnadirReportajeView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Anadir Reportajes");
		frame.setName("Anadir Reportajes");
		frame.setBounds(100, 100, 1306, 762);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblListaDeEventos = new JLabel("Lista de Eventos sin Reportaje");
		lblListaDeEventos.setBounds(24, 144, 219, 29);
		frame.getContentPane().add(lblListaDeEventos);
		
		JLabel lblTtulo = new JLabel("T�tulo");
		lblTtulo.setBounds(523, 123, 46, 14);
		frame.getContentPane().add(lblTtulo);
		
		textFieldTitulo = new JTextField();
		textFieldTitulo.setBounds(523, 148, 134, 20);
		frame.getContentPane().add(textFieldTitulo);
		textFieldTitulo.setColumns(10);
		
		textFieldSubtitulo = new JTextField();
		textFieldSubtitulo.setBounds(523, 216, 219, 20);
		frame.getContentPane().add(textFieldSubtitulo);
		textFieldSubtitulo.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Subt\u00EDtulo");
		lblNewLabel.setBounds(523, 191, 46, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Cuerpo");
		lblNewLabel_1.setBounds(523, 247, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		textPaneCuerpo = new JTextPane();
		textPaneCuerpo.setBounds(525, 272, 243, 119);
		frame.getContentPane().add(textPaneCuerpo);
		
		btnAnadirReportaje = new JButton("A�adir Reportaje");
		btnAnadirReportaje.setEnabled(false);
		btnAnadirReportaje.setBounds(1009, 420, 115, 20);
		frame.getContentPane().add(btnAnadirReportaje);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 179, 443, 230);
		frame.getContentPane().add(scrollPane);
		
		tableEventos = new JTable();
		tableEventos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableEventos.setDefaultEditor(Object.class, null); //readonly
		scrollPane.setViewportView(tableEventos);
		
		textFieldIdReportero = new JTextField();
		textFieldIdReportero.setBounds(24, 53, 134, 20);
		frame.getContentPane().add(textFieldIdReportero);
		textFieldIdReportero.setColumns(10);
		
		JLabel lblIdreportero = new JLabel("Id_Reportero");
		lblIdreportero.setBounds(24, 27, 86, 15);
		frame.getContentPane().add(lblIdreportero);
		
		btnAceptar = new JButton("Aceptar");
		
		btnAceptar.setBounds(69, 84, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		lblMultimedia = new JLabel("Multimedia");
		lblMultimedia.setBounds(815, 66, 106, 14);
		frame.getContentPane().add(lblMultimedia);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(816, 86, 452, 323);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		textFieldRuta = new JTextField();
		textFieldRuta.setBounds(10, 58, 162, 20);
		textFieldRuta.setColumns(10);
		panel.add(textFieldRuta);
		
		btnAnadirMultimedia = new JButton("A\u00F1adir Multimedia");
		btnAnadirMultimedia.setBounds(10, 89, 136, 20);
		panel.add(btnAnadirMultimedia);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(338, 205, 89, 23);
		panel.add(btnEliminar);
		btnEliminar.setEnabled(false);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(199, 60, 228, 119);
		panel.add(scrollPane_1);
		
		tableMultimedia = new JTable();
		tableMultimedia.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableMultimedia.setDefaultEditor(Object.class, null); //readonly
		scrollPane_1.setViewportView(tableMultimedia);
		
		JLabel lblListaDeContenido = new JLabel("Lista de Contenido Multimedia");
		lblListaDeContenido.setBounds(199, 33, 206, 14);
		panel.add(lblListaDeContenido);
		
		JLabel lblRuta = new JLabel("Ruta");
		lblRuta.setBounds(10, 33, 46, 14);
		panel.add(lblRuta);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(492, 88, 303, 321);
		frame.getContentPane().add(panel_1);
		
		JLabel lblRevisin = new JLabel("Revisi\u00F3n");
		lblRevisin.setBounds(492, 453, 96, 14);
		frame.getContentPane().add(lblRevisin);
		
		panelRevision = new JPanel();
		panelRevision.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelRevision.setBounds(492, 472, 303, 223);
		frame.getContentPane().add(panelRevision);
		panelRevision.setLayout(null);
		
		textPaneComentario = new JTextPane();
		textPaneComentario.setBounds(35, 36, 245, 131);
		panelRevision.add(textPaneComentario);
		
		chckbxRevisado = new JCheckBox("Revisado");
		chckbxRevisado.setEnabled(false);
		chckbxRevisado.setBounds(35, 186, 97, 23);
		panelRevision.add(chckbxRevisado);
		
		btnGuardarComentario = new JButton("Guardar Comentario");
		btnGuardarComentario.setEnabled(false);
		btnGuardarComentario.setBounds(157, 187, 136, 20);
		panelRevision.add(btnGuardarComentario);
		
		JLabel lblComentarioSobreEl = new JLabel("Comentario sobre el reportaje");
		lblComentarioSobreEl.setBounds(35, 11, 195, 14);
		panelRevision.add(lblComentarioSobreEl);
		
		JLabel lblListaDeReportajes = new JLabel("Lista de eventos con Reportaje sin Finalizar");
		lblListaDeReportajes.setBounds(24, 453, 219, 14);
		frame.getContentPane().add(lblListaDeReportajes);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(24, 473, 443, 222);
		frame.getContentPane().add(scrollPane_2);
		
		tableEventosConReportajes = new JTable();
		tableEventosConReportajes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableEventosConReportajes.setDefaultEditor(Object.class, null); //readonly
		scrollPane_2.setViewportView(tableEventosConReportajes);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(821, 472, 447, 223);
		frame.getContentPane().add(scrollPane_3);
		
		tableComentarios = new JTable();
		tableComentarios.setRowSelectionAllowed(false);
		tableComentarios.setColumnSelectionAllowed(false);
		tableComentarios.setCellSelectionEnabled(false);
		tableComentarios.setDefaultEditor(Object.class, null); //readonly

		scrollPane_3.setViewportView(tableComentarios);
		
		JLabel label = new JLabel("Lista de Comentarios sodre el reportaje");
		label.setBounds(823, 453, 235, 14);
		frame.getContentPane().add(label);
		
		JLabel lblAadirReportaje = new JLabel("A\u00F1adir Reportaje");
		lblAadirReportaje.setBounds(492, 66, 106, 14);
		frame.getContentPane().add(lblAadirReportaje);
		
		btnActualizarReportaje = new JButton("Actualizar Reportaje");
		btnActualizarReportaje.setEnabled(false);
		btnActualizarReportaje.setBounds(1134, 420, 134, 20);
		frame.getContentPane().add(btnActualizarReportaje);
		
		btnNewButton = new JButton("Finalizar Reportaje");
		btnNewButton.setEnabled(false);
		
		btnNewButton.setBounds(815, 419, 145, 22);
		frame.getContentPane().add(btnNewButton);
	}

	public JButton getBtnActualizarReportaje() {
		return btnActualizarReportaje;
	}

	public void setBtnActualizarReportaje(JButton btnActualizarReportaje) {
		this.btnActualizarReportaje = btnActualizarReportaje;
	}

	public JTable getTableComentarios() {
		return tableComentarios;
	}

	public void setTableComentarios(JTable tableComentarios) {
		this.tableComentarios = tableComentarios;
	}

	public JTable getTableEventosConReportajes() {
		return tableEventosConReportajes;
	}

	public void setTableEventosConReportajes(JTable tableEventosConReportajes) {
		this.tableEventosConReportajes = tableEventosConReportajes;
	}

	public JScrollPane getScrollPane_1() {
		return scrollPane_1;
	}

	public void setScrollPane_1(JScrollPane scrollPane_1) {
		this.scrollPane_1 = scrollPane_1;
	}

	public JTextPane getTextPaneComentario() {
		return textPaneComentario;
	}

	public void setTextPaneComentario(JTextPane textPane) {
		this.textPaneComentario = textPane;
	}

	public JCheckBox getChckbxRevisado() {
		return chckbxRevisado;
	}

	public void setChckbxRevisado(JCheckBox chckbxRevisado) {
		this.chckbxRevisado = chckbxRevisado;
	}

	public JButton getBtnGuardarComentario() {
		return btnGuardarComentario;
	}

	public void setBtnGuardarComentario(JButton btnGuardarComentario) {
		this.btnGuardarComentario = btnGuardarComentario;
	}

	public JLabel getLblMultimedia() {
		return lblMultimedia;
	}

	public void setLblMultimedia(JLabel lblMultimedia) {
		this.lblMultimedia = lblMultimedia;
	}


	public JTable getTableMultimedia() {
		return tableMultimedia;
	}

	public void setTableMultimedia(JTable tableMultimedia) {
		this.tableMultimedia = tableMultimedia;
	}


	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JPanel getPanel_1() {
		return panel_1;
	}

	public void setPanel_1(JPanel panel_1) {
		this.panel_1 = panel_1;
	}

	public JTextField getTextFieldRuta() {
		return textFieldRuta;
	}

	public void setTextFieldRuta(JTextField textFieldRuta) {
		this.textFieldRuta = textFieldRuta;
	}


	public JButton getBtnAnadirMultimedia() {
		return btnAnadirMultimedia;
	}

	public void setBtnAnadirMultimedia(JButton btnAnadirMultimedia) {
		this.btnAnadirMultimedia = btnAnadirMultimedia;
	}
}
