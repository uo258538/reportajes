package vista.reportajes;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import lombok.Getter;
import lombok.Setter;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;


public class InformeEventosReporteroView {

	private JFrame frame;
	private JTextField textFieldIdReportero;
	private JTable tableEventos;
	private JTextField textFieldTotalDietas;
	private JButton btnAceptar;
	private JLabel lblFechaInicio;
	private JLabel lblFechaFin;
	private JTextField textFieldFechaIni;
	private JTextField textFieldFechaFin;
	private JButton btnAplicarFiltroFecha;

	/**
	 * Create the application.
	 */
	public InformeEventosReporteroView() {
		initialize();
	}

	
	public JFrame getFrame() {
		return frame;
	}


	public void setFrame(JFrame frame) {
		this.frame = frame;
	}


	public JTextField getTextFieldIdReportero() {
		return textFieldIdReportero;
	}


	public void setTextFieldIdReportero(JTextField textFieldIdReportero) {
		this.textFieldIdReportero = textFieldIdReportero;
	}


	public JTable getTableEventos() {
		return tableEventos;
	}


	public void setTableEventos(JTable tableEventos) {
		this.tableEventos = tableEventos;
	}


	public JTextField getTextFieldTotalDietas() {
		return textFieldTotalDietas;
	}


	public void setTextFieldTotalDietas(JTextField textFieldTotalDietas) {
		this.textFieldTotalDietas = textFieldTotalDietas;
	}


	public JButton getBtnAceptar() {
		return btnAceptar;
	}


	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}


	public JLabel getLblFechaInicio() {
		return lblFechaInicio;
	}


	public void setLblFechaInicio(JLabel lblFechaInicio) {
		this.lblFechaInicio = lblFechaInicio;
	}


	public JLabel getLblFechaFin() {
		return lblFechaFin;
	}


	public void setLblFechaFin(JLabel lblFechaFin) {
		this.lblFechaFin = lblFechaFin;
	}


	public JTextField getTextFieldFechaIni() {
		return textFieldFechaIni;
	}


	public void setTextFieldFechaIni(JTextField textFieldFechaIni) {
		this.textFieldFechaIni = textFieldFechaIni;
	}


	public JTextField getTextFieldFechaFin() {
		return textFieldFechaFin;
	}


	public void setTextFieldFechaFin(JTextField textFieldFechaFin) {
		this.textFieldFechaFin = textFieldFechaFin;
	}


	public JButton getBtnAplicarFiltroFecha() {
		return btnAplicarFiltroFecha;
	}


	public void setBtnAplicarFiltroFecha(JButton btnAplicarFiltroFecha) {
		this.btnAplicarFiltroFecha = btnAplicarFiltroFecha;
	}


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 550, 577);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Informe Eventos de un Reportero");
		frame.setName("Informe Eventos de un Reportero");
		
		JLabel lblIdReportero = new JLabel("Id Reportero");
		lblIdReportero.setBounds(10, 11, 125, 14);
		frame.getContentPane().add(lblIdReportero);
		
		textFieldIdReportero = new JTextField();
		textFieldIdReportero.setBounds(10, 36, 89, 20);
		frame.getContentPane().add(textFieldIdReportero);
		textFieldIdReportero.setColumns(10);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(10, 65, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		JLabel lblListaDeEventos = new JLabel("Lista de Eventos");
		lblListaDeEventos.setBounds(10, 212, 175, 14);
		frame.getContentPane().add(lblListaDeEventos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 237, 497, 188);
		frame.getContentPane().add(scrollPane);
		
		tableEventos = new JTable();
		scrollPane.setViewportView(tableEventos);
		
		JLabel lblImporteTotalPor = new JLabel("Importe total por dietas en euros");
		lblImporteTotalPor.setBounds(10, 435, 256, 14);
		frame.getContentPane().add(lblImporteTotalPor);
		
		textFieldTotalDietas = new JTextField();
		textFieldTotalDietas.setEditable(false);
		textFieldTotalDietas.setBounds(10, 460, 125, 20);
		frame.getContentPane().add(textFieldTotalDietas);
		textFieldTotalDietas.setColumns(10);
		
		lblFechaInicio = new JLabel("Fecha Inicio");
		lblFechaInicio.setBounds(10, 110, 89, 14);
		frame.getContentPane().add(lblFechaInicio);
		
		lblFechaFin = new JLabel("Fecha Fin");
		lblFechaFin.setBounds(109, 110, 96, 14);
		frame.getContentPane().add(lblFechaFin);
		
		textFieldFechaIni = new JTextField();
		textFieldFechaIni.setBounds(10, 135, 86, 20);
		frame.getContentPane().add(textFieldFechaIni);
		textFieldFechaIni.setColumns(10);
		
		textFieldFechaFin = new JTextField();
		textFieldFechaFin.setBounds(99, 135, 86, 20);
		frame.getContentPane().add(textFieldFechaFin);
		textFieldFechaFin.setColumns(10);
		
		btnAplicarFiltroFecha = new JButton("Aplicar Filtro Fecha");
		btnAplicarFiltroFecha.setEnabled(false);
		btnAplicarFiltroFecha.setBounds(46, 166, 139, 23);
		frame.getContentPane().add(btnAplicarFiltroFecha);
	}
}
