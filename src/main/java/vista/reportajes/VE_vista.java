package vista.reportajes;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

public class VE_vista {

	private JFrame frmVisualizarReporterosAsignados;
	private JTable tabla_eventos;
	private JTable tabla_reporteros;
	private JLabel lblReporteros;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VE_vista window = new VE_vista();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	*/

	/**
	 * Create the application.
	 */
	public VE_vista() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmVisualizarReporterosAsignados = new JFrame();
		frmVisualizarReporterosAsignados.setTitle("Visualizar reporteros asignados a eventos");
		frmVisualizarReporterosAsignados.setBounds(100, 100, 450, 300);
		frmVisualizarReporterosAsignados.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmVisualizarReporterosAsignados.getContentPane().setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 84, 206, 119);
		frmVisualizarReporterosAsignados.getContentPane().add(scrollPane);

		tabla_eventos = new JTable();
		scrollPane.setViewportView(tabla_eventos);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(254, 84, 114, 119);
		frmVisualizarReporterosAsignados.getContentPane().add(scrollPane_1);
		
		tabla_reporteros = new JTable();
		tabla_eventos.setModel(tableModel);
		tabla_reporteros.setModel(tableModel);
		tabla_reporteros.setVisible(false);
		scrollPane_1.setVisible(false);
		scrollPane_1.setViewportView(tabla_reporteros);
		
		
		JLabel lblEventos = new JLabel("Eventos");
		lblEventos.setBounds(50, 59, 87, 14);
		frmVisualizarReporterosAsignados.getContentPane().add(lblEventos);
		
		lblReporteros = new JLabel("Reporteros");
		lblReporteros.setVisible(false);
		lblReporteros.setBounds(254, 59, 130, 14);
		frmVisualizarReporterosAsignados.getContentPane().add(lblReporteros);
	}
	
	public JFrame getFrame() { return this.frmVisualizarReporterosAsignados;}
	public JTable getTablaEventos() { return this.tabla_eventos;}
	public JTable getTablaReporteros() { return this.tabla_reporteros;}
	public JLabel getLabelReporteros() { return this.lblReporteros;}
	public JScrollPane getScrollPanel() { return this.scrollPane_1;}
	
	DefaultTableModel tableModel = new DefaultTableModel() {
	    @Override
	    public boolean isCellEditable(int row, int column) {
	       //all cells false
	       return false;
	    }
	};
}
