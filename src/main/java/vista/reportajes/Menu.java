package vista.reportajes;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

public class Menu {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	*/

	/**
	 * Create the application.
	 */
	public Menu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 645, 330);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnAsignarReporteros = new JButton("Asignar Reporteros");
		btnAsignarReporteros.setBounds(443, 70, 168, 23);
		frame.getContentPane().add(btnAsignarReporteros);
		
		JButton btnCrearOfrecimientos = new JButton("Crear ofrecimientos");
		btnCrearOfrecimientos.setBounds(443, 104, 168, 23);
		frame.getContentPane().add(btnCrearOfrecimientos);
		
		JButton btnGestinAccesoA = new JButton("Gesti\u00F3n acceso a reportajes");
		btnGestinAccesoA.setBounds(10, 153, 168, 23);
		frame.getContentPane().add(btnGestinAccesoA);
		
		JButton btnInformeReportajes = new JButton("Informe Reportajes");
		btnInformeReportajes.setBounds(10, 195, 168, 23);
		frame.getContentPane().add(btnInformeReportajes);
		
		JButton btnGestionarOfrecimientos = new JButton("Gestionar Ofrecimientos");
		btnGestionarOfrecimientos.setBounds(217, 70, 164, 23);
		frame.getContentPane().add(btnGestionarOfrecimientos);
		
		JButton btnInformeEventosGestor = new JButton("Informe Eventos Gestor");
		btnInformeEventosGestor.setBounds(443, 214, 164, 23);
		frame.getContentPane().add(btnInformeEventosGestor);
		
		JButton btnInformeEventosReportero = new JButton("Informe Eventos Reportero");
		btnInformeEventosReportero.setBounds(10, 229, 168, 23);
		frame.getContentPane().add(btnInformeEventosReportero);
		
		JLabel lblMen = new JLabel("Men\u00FA");
		lblMen.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblMen.setBounds(10, 22, 80, 14);
		frame.getContentPane().add(lblMen);
		
		JLabel lblReporteros = new JLabel("Reporteros");
		lblReporteros.setBounds(48, 47, 80, 14);
		frame.getContentPane().add(lblReporteros);
		
		JLabel lblEmpresasDeComunicacin = new JLabel("Empresas de Comunicaci\u00F3n");
		lblEmpresasDeComunicacin.setBounds(229, 47, 168, 14);
		frame.getContentPane().add(lblEmpresasDeComunicacin);
		
		JLabel lblGestor = new JLabel("Gestor");
		lblGestor.setBounds(433, 47, 46, 14);
		frame.getContentPane().add(lblGestor);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(194, 47, 1, 233);
		frame.getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setBounds(407, 46, 1, 234);
		frame.getContentPane().add(separator_1);
	}
}
