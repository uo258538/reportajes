package vista.reportajes;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class GREP_vista {

	private JFrame frmGestionarReportajes;
	private JTable tabla_eventos_of;
	private JButton btnRechazar;
	private JButton btnAceptar;
	private JLabel lblquieres;
	private JComboBox comboBox;
	private JSpinner spinner;
	private JSpinner spinner_1;
	private JButton btnAplicar;

	/**
	 * Launch the application.
	 */
	
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GREP_vista window = new GREP_vista();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	*/

	/**
	 * Create the application.
	 */
	public GREP_vista() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestionarReportajes = new JFrame();
		frmGestionarReportajes.setTitle("Gestionar reportajes");
		frmGestionarReportajes.setBounds(100, 100, 802, 398);
		frmGestionarReportajes.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmGestionarReportajes.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(32, 86, 427, 140);
		frmGestionarReportajes.getContentPane().add(scrollPane);
		
		tabla_eventos_of = new JTable();
		tabla_eventos_of.setModel(tableModel);
		tabla_eventos_of.setDefaultEditor(Object.class, null); 
		scrollPane.setViewportView(tabla_eventos_of);
		
		JLabel lblEventosOfrecidos = new JLabel("Eventos ofrecidos");
		lblEventosOfrecidos.setBounds(32, 61, 261, 14);
		frmGestionarReportajes.getContentPane().add(lblEventosOfrecidos);
		
		lblquieres = new JLabel("\u00BFQuieres aceptar o rechazar este ofrecimiento?");
		lblquieres.setVisible(false);
		lblquieres.setBounds(508, 113, 268, 14);
		frmGestionarReportajes.getContentPane().add(lblquieres);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setVisible(false);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAceptar.setBounds(496, 153, 89, 23);
		frmGestionarReportajes.getContentPane().add(btnAceptar);
		
		btnRechazar = new JButton("Rechazar");
		btnRechazar.setVisible(false);
		btnRechazar.setBounds(651, 153, 111, 23);
		frmGestionarReportajes.getContentPane().add(btnRechazar);
		
		lblIdEmpresa = new JLabel("ID Empresa:");
		lblIdEmpresa.setBounds(32, 22, 75, 14);
		frmGestionarReportajes.getContentPane().add(lblIdEmpresa);
		
		TFid_empresa = new JTextField();
		TFid_empresa.setBounds(104, 19, 86, 20);
		frmGestionarReportajes.getContentPane().add(TFid_empresa);
		TFid_empresa.setColumns(10);
		
		btnAceptar_ID = new JButton("Aceptar");
		btnAceptar_ID.setBounds(204, 18, 89, 23);
		frmGestionarReportajes.getContentPane().add(btnAceptar_ID);
		
		comboBox = new JComboBox();
		
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Ninguno", "Por M\u00E1ximo", "Por M\u00EDnimo", "Por M\u00E1ximo y M\u00EDnimo"}));
		comboBox.setBounds(104, 242, 145, 20);
		frmGestionarReportajes.getContentPane().add(comboBox);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(0), null, null, new Integer(10)));
		spinner.setEnabled(false);
		spinner.setBounds(88, 273, 55, 20);
		frmGestionarReportajes.getContentPane().add(spinner);
		
		JLabel lblMnimo = new JLabel("M\u00EDnimo:");
		lblMnimo.setBounds(32, 276, 46, 14);
		frmGestionarReportajes.getContentPane().add(lblMnimo);
		
		JLabel lblMximo = new JLabel("M\u00E1ximo:");
		lblMximo.setBounds(173, 276, 46, 14);
		frmGestionarReportajes.getContentPane().add(lblMximo);
		
		spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerNumberModel(new Integer(500), null, null, new Integer(10)));
		spinner_1.setEnabled(false);
		spinner_1.setBounds(229, 273, 55, 20);
		frmGestionarReportajes.getContentPane().add(spinner_1);
		
		btnAplicar = new JButton("Aplicar");
		btnAplicar.setEnabled(false);
		
		btnAplicar.setBounds(311, 272, 89, 23);
		frmGestionarReportajes.getContentPane().add(btnAplicar);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setEnabled(false);
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Todas"}));
		comboBox_1.setBounds(104, 304, 145, 20);
		frmGestionarReportajes.getContentPane().add(comboBox_1);
		
		lblTemtica = new JLabel("Tem\u00E1tica:");
		lblTemtica.setBounds(32, 307, 60, 14);
		frmGestionarReportajes.getContentPane().add(lblTemtica);
		
		lblPrecio = new JLabel("Precio:");
		lblPrecio.setBounds(32, 245, 46, 14);
		frmGestionarReportajes.getContentPane().add(lblPrecio);
	}
	
	public JFrame getFrame() { return this.frmGestionarReportajes;}
	public JTable getTablaOfrecimientos() { return this.tabla_eventos_of;}
	public JButton getBotonAceptar() { return this.btnAceptar;}
	public JButton getBotonRechazar() { return this.btnRechazar;}
	public JLabel getLabelOpciones() { return this.lblquieres;}
	public JTextField getTFid_empresa() { return this.TFid_empresa;}
	public JButton getBotonID() { return this.btnAceptar_ID;}
	public JComboBox getFiltroPrecio() { return this.comboBox;}
	public JButton btnAplicar() { return this.btnAplicar;}
	public JSpinner getMinimoFiltro() { return this.spinner;}
	public JSpinner getMaximoFiltro() { return this.spinner_1;}
	public JComboBox getComboTematica() { return this.comboBox_1;}
	
	DefaultTableModel tableModel = new DefaultTableModel() {
	    @Override
	    public boolean isCellEditable(int row, int column) {
	       //all cells false
	       return false;
	    }
	};
	private JLabel lblIdEmpresa;
	private JTextField TFid_empresa;
	private JButton btnAceptar_ID;
	private JComboBox comboBox_1;
	private JLabel lblTemtica;
	private JLabel lblPrecio;
}
