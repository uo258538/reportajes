--Datos para carga inicial de la base de datos
delete from reportero;
insert into reportero(id_reportero,nombre,id_contrato, id_lugar) values 
	(201,'Juan',3,2),
	(202,'Andres',2,2),
	(203,'Antonio',3,3),
	(204,'Eva',1,4),
	(205,'Sara',2,2),
	(206,'Pedro',3,2),
	(207,'Javier',3,4),
	(208,'Ismael',1,2),
	(209,'Lucia',3,1),
	(210,'Ana',3,1),
	(211,'Alberto',3,1),
	(212,'Miguel',3,2);


delete from reportaje;
insert into reportaje(id_reportaje,titulo,subtitulo,cuerpo,finalizado) values 
	(300,'Entrega de los Goya','Los Goya','cuerpo',0),
	(301,'Feria de Muestras 2019','Novedades Feria de Muestras','cuerpo',1),
	(302,'Partido Sporting-Oviedo','Futbol','cuerpo',1),
	(303,'Jardin Botanico','Naturalenza','cuerpo',1),
	(304,'Concierto','Concierto-subtuitulo','cuerpo',1);


delete from evento;
insert into evento(id_evento,diaInicio,diaFin,descripcion,titulo,id_reportaje,id_agencia,equipo_terminado,precio,id_lugar) values 
	(101,'2019-03-20','2019-03-25','descripcion premios goya','Premios Goya',300,401,1,200,1),
	(102,'2019-10-01','2019-10-03','descripcion Feria de muestras','Feria de muestras',301,401,0,100,2),
	(103,'2019-10-10','2019-10-13','descripcion inaguracion museo','Inaguracion museo',NULL,402,1,150,3),
	(104,'2019-05-10','2019-05-14','descripcion jardin botanico','Jardin Botanico',303,403,0,300,4),
	(105,'2019-11-29','2019-11-31','descripcion concierto','Concierto',NULL,401,0,250,4),
	(106,'2019-03-11','2019-03-14','descripcion entrevista','Entrevista',NULL,402,0,320,2),
	(107,'2019-03-15','2019-03-17','descripcion partido','Partido',NULL,402,0,100,1),
	(108,'2019-03-13','2019-03-19','descripcion festival','Festival',NULL,402,0,100,3),
	(109,'2019-03-03','2019-03-04','despcripcion partido','Partido',302,401,1,100,2),
	(120,'2020-01-03','2020-01-07','despcripcion carnaval','Carnaval',NULL,401,0,200,1),
	(150,'2020-01-10','2020-01-15','despcripcion carrera','Carrera',NULL,401,1,200,1),
	(151,'2020-01-16','2020-01-21','despcripcion cine','Cine',NULL,402,0,200,1),
	(152,'2020-01-22','2020-01-27','despcripcion gran premio','Gran Premio',NULL,401,0,200,1);

	


delete from agencia_prensa;
insert into agencia_prensa(id_agencia,nombre) values 
	(401,'Agencia1'),
	(402,'Agencia2'),
	(403,'Agencia3');

delete from empresa_comunicacion;
insert into empresa_comunicacion(id_empresa,nombre) values 
	(501,'Empresa1'),
	(502,'Empresa2'),
	(503,'Empresa3'),
	(504,'Empresa4'),
	(505,'Empresa5'),
	(506, 'Empresa6'),
	(507, 'Empresa7'),
	(508, 'Empresa8'),
	(509, 'Empresa9'),
	(510, 'Empresa10');

delete from ofrecimiento;
insert into ofrecimiento(id_ofrecimiento,aceptado,acceso,pagado,id_evento,id_empresa) values
	(601,1,1,0,101,501),
	(602,1,1,1,102,503),
	(603,NULL,0,0,102,504),
	(604,1,0,1,104,501),
	(605,NULL,0,0,104,502),
	(606,1,0,1,109,501),
	(607,1,0,0,109,502),
	(608,1,1,1,102,501),
	(609,NULL,0,0,101,505),
	(610,NULL,0,0,102,505),
	(611,NULL,0,0,103,505),
	(612,NULL,0,0,104,505),
	(613,NULL,0,0,105,505),
	(614,NULL,0,0,106,505);


delete from equipo;
insert into equipo(id_evento,id_reportero,responsable,comentario,revisado) values
	(101,201,1,'Comentario',0),
	(101,204,0,'Comentario',1),
	(102,204,1,NULL,0),
	(102,203,0,'Comentario',0),
	(102,202,0,'Comentario',1),
	(103,204,0,NULL,0),
	(104,204,1,NULL,0),
	(109,205,1,NULL,1),
	(109,208,0,'Comentario',1),
	(109,209,0,'hola',1),
	(106,204,1,'comentario',1),
	(150,212,1,NULL,0);


delete from multimedia;
insert into multimedia(id_multimedia,tipo,ruta,id_reportaje) values
	(701,'foto','ruta1',300),
	(702,'foto','ruta2',300),
	(703,'foto','ruta3',301),
	(704,'video','ruta4',301),
	(705,'foto','ruta5',NULL),
	(706,'video','ruta6',NULL),
	(707,'video','ruta7',300);

delete from tema;
insert into tema(id_tema,nombre) values
	(801,'deportes'),
	(802,'investigacion'),
	(803,'politica'),
	(805,'economia'),
	(806,'salud');

delete from temaevento;
insert into temaevento(id_evento,id_tema) values
	(101,801),
	(102,802),
	(103,803),
	(104,804),
	(105,805),
	(106,806),
	(103,801),
	(105,802),
	(101,803),
	(102,804),
	(104,805),
	(108,803),
	(108,805),
	(107,801),
	(109,806),
	(120,801),
	(120,802),
	(120,803),
	(150,801),
	(150,802),
	(150,803),
	(151,801),
	(151,802),
	(151,803),
	(152,801),
	(152,802),
	(152,803);

delete from temareportero;
insert into temareportero(id_reportero,id_tema) values
	(201,801),
	(201,802),
	(202,803),
	(203,804),
	(204,805),
	(206,806),
	(208,801),
	(205,802),
	(201,803),
	(207,801),
	(209,805),
	(209,806),
	(209,804),
	(210,801),
	(211,802),
	(212,803);

	

delete from rol;
insert into rol(id_rol,nombre) values
	(1,'base'),
	(2,'camarografo'),
	(3,'grafico');

delete from rolreportero;
insert into rolreportero(id_rol,id_reportero) values
	(1,201),
	(2,202),
	(3,203),
	(2,204),
	(1,205),
	(2,206),
	(3,207),
	(2,208),
	(1,209),
	(3,210),
	(2,211),
	(1,212),
	(3,201),
	(1,204),
	(3,206),
	(3,209);


delete from contrato;
insert into contrato(id_contrato,nombre) values
	(1,'fin de semana'),
	(2,'laborables'),
	(3,'todos los dias');
	

	
delete from lugar;
insert into lugar (id_lugar, pais, region,dieta_alimentacion,dieta_alojamiento) values
	(1, 'espana', 'asturias',50,100),
	(2, 'espana', 'galicia',50,80),
	(3, 'espana', 'madrid',50,80),
	(4, 'espana', 'barcelona',50,80);

delete from tarifa;
insert into tarifa (id_empresa, id_agencia, tipo,pagado) values
	
	(506, 401, 'plana',1),
	(507, 401, 'plana',0),
	(508, 401, 'plana',1),
	(509, 402, 'plana',1),
	(510, 401, 'plana',1);


