drop table reportaje;
create table reportaje(id_reportaje int primary key, titulo varchar(32), subtitulo varchar(60),cuerpo varchar(250), finalizado int);

drop table evento;
create table evento (id_evento int primary key, diaInicio date,diaFin date , descripcion varchar(60), titulo varchar(32),id_reportaje int,
id_agencia, equipo_terminado int, precio int, id_lugar int, FOREIGN KEY(id_reportaje) REFERENCES reportaje(id_reportaje),
FOREIGN KEY(id_agencia) REFERENCES agencia_prensa(id_agencia), FOREIGN KEY(id_lugar) REFERENCES lugar(id_lugar));

drop table reportero;
create table reportero (id_reportero int primary key, nombre varchar(32), id_contrato int, id_lugar, FOREIGN KEY(id_contrato) REFERENCES contrato (id_contrato),
FOREIGN KEY(id_lugar) REFERENCES lugar(id_lugar));

drop table agencia_prensa;
create table agencia_prensa(id_agencia int primary key,nombre varchar(32));

drop table empresa_comunicacion;
create table empresa_comunicacion(id_empresa int primary key,nombre varchar(32));

drop table ofrecimiento;
create table ofrecimiento(id_ofrecimiento int primary key,aceptado int,acceso int,pagado int,id_evento int, id_empresa int,
FOREIGN KEY(id_evento) REFERENCES evento(id_evento), FOREIGN KEY(id_empresa) REFERENCES empresa_comunicacion(id_empresa));

drop table equipo;
create table equipo(id_evento int, id_reportero int, responsable int, comentario varchar(120), revisado int,FOREIGN KEY(id_evento) REFERENCES evento (id_evento),FOREIGN KEY(id_reportero) REFERENCES reportero(id_reportero));

drop table multimedia;
create table multimedia(id_multimedia int primary key,tipo varchar(32),ruta varchar(32), id_reportaje int ,FOREIGN KEY(id_reportaje) REFERENCES reportaje(id_reportaje));


drop table tema;
create table tema(id_tema int primary key,nombre varchar(32));

drop table temareportero;
create table temareportero(id_reportero int NOT NULL, id_tema int NOT NULL, FOREIGN KEY(id_tema) REFERENCES tema(id_tema), FOREIGN KEY(id_reportero) REFERENCES reportero(id_reportero), PRIMARY KEY (id_reportero,id_tema));

drop table temaevento;
create table temaevento(id_evento int NOT NULL, id_tema int NOT NULL, FOREIGN KEY(id_tema) REFERENCES tema(id_tema), FOREIGN KEY(id_evento) REFERENCES evento(id_evento), PRIMARY KEY (id_evento,id_tema));

drop table rol;
create table rol(id_rol int primary key,nombre varchar(32));

drop table rolreportero;
create table rolreportero(id_rol int NOT NULL, id_reportero int NOT NULL, FOREIGN KEY(id_rol) REFERENCES rol(id_rol), FOREIGN KEY(id_reportero) REFERENCES reportero(id_reportero), PRIMARY KEY (id_rol,id_reportero));

drop table contrato;
create table contrato(id_contrato int primary key,nombre varchar(32));

drop table lugar;
create table lugar(id_lugar int, pais varchar(20),region varchar(20),dieta_alimentacion int,dieta_alojamiento int);

drop table tarifa;
create table tarifa(id_empresa NOT NULL, id_agencia NOT NULL, tipo varchar(22), pagado int, FOREIGN KEY(id_empresa) REFERENCES empresa_comunicacion(id_empresa),
FOREIGN KEY(id_agencia) REFERENCES agencia_prensa(id_agencia));

